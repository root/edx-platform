from django import forms

from models import Student_Identity


import re
from mooc.models import Student_Document
from mooc.models import Course, Institute_Course, Institute_Identity
from django.core.validators import validate_email


class UploadFileForm(forms.ModelForm):

    class Meta:
        model = Student_Identity


class LocalAdminEmailForm(forms.Form):
    ''' 
    This form view details for  Request for Local Admin  
    Email field used  
    '''
    error_messages_doc = {
        'email': ("Enter a valid Email"), 
    }
    
    emailid = forms.CharField(max_length=75, label='Enter Email')
    

    def __init__(self, *args, **kwargs):
        super(LocalAdminEmailForm, self).__init__(*args, **kwargs)
        self.fields['emailid'].error_messages = {'required': 'Enter a valid e-mail address'}
          

    def clean_emailid(self):
        emailid = None
        try:
            emailid = self.cleaned_data['emailid']
            validate_email(emailid)
        except Exception as e:
            raise forms.ValidationError("Enter a valid e-mail address") 
         
        # your other validations go here.
        return emailid



class StudentProofIdForm(forms.Form):
    ''' 
       This form view details select course/institute with specific identity proof  
       StudentDocument File upload  
    '''
    error_messages_doc = {
        'ainstitute_identity': ("Select an Identity Proof"),
        'aproofnumber': ("Enter a valid ID Proof number"), 
        'adocfile': ("Upload a file in .jpg, .jpeg, .png, .gif format"),
        'adocfilesize': ("Please keep filesize under 2.5MB"),
    }
    
    identity = forms.ChoiceField(choices=(), label='Identity Proof')
    proofnumber = forms.CharField(max_length=100, label='Enter Proof ID')
    docfile = forms.FileField(label='Select a file')
    filename = forms.CharField(widget=forms.HiddenInput())
    

    def __init__(self, *args, **kwargs):
        institute_idn = kwargs.pop('institute_idn', None)
        super(StudentProofIdForm, self).__init__(*args, **kwargs)
        self.content_types = ['image/jpg', 'image/jpeg' , 'image/png' , 'image/gif' ]
        self.max_upload_size = 2621440 # 1024 # 2.5MB 
        self.fields['filename'].initial = 'None.png'
        self.fields['identity'].error_messages = {'required': 'Select an Identity Proof'}
        choices = []
        choices.extend([('', 'Select Identity Proof')])
        choices.extend([(int(iid.id), unicode(iid.identity.name)) for iid in Institute_Identity.objects.filter(institute_id=institute_idn)])
        self.fields['identity'].choices = choices
        self.fields['proofnumber'].error_messages = {'required': 'Enter a valid ID Proof number'}
        self.fields['docfile'].error_messages = {'required': 'Choose a file to upload'}
          

    def clean_identity(self):
        identity = None
        try:
            identity = self.cleaned_data['identity']
            if identity and identity in [None, '', 'Select Identity Proof',]:
                raise Exception
        except Exception as e:
            raise forms.ValidationError("Select an Identity Proof") 

         
        # your other validations go here.
        return identity

    def clean_docfile(self):
        docfile = self.cleaned_data['docfile']   

        try:
            content_type = docfile.content_type
            if (content_type in self.content_types): 
                if docfile.size > self.max_upload_size: # 
                    raise forms.ValidationError(self.error_messages_doc['adocfilesize'])
            else:
                raise Exception
             
        except Exception as e:
            raise forms.ValidationError(self.error_messages_doc['adocfile']) 

        
        # your other validations go here.
        return docfile

    def clean_proofnumber(self):
        proofnumber = None
        try:
            proofnumber = self.cleaned_data['proofnumber']
            if proofnumber:
                if len(proofnumber) == 0 :
                   raise Exception
                valid = re.match('^[\w-]+$', proofnumber) is not None
                if valid is False:
                   raise Exception
        except Exception as e:
            raise forms.ValidationError("Enter a valid ID Proof number") 

         
        # your other validations go here.
        return proofnumber


    

class StudentCourseInstituteForm(forms.Form):
    ''' 
    This form view details select course/institute  
    '''
    error_messages_doc = {
        'acourse': ("Select a Course"),
        'ainstitute_course': ("Select a Institute"),
    }
    
    course = forms.ModelChoiceField(queryset=Course.objects.all(), label='Course', empty_label="Select Course")
    institute = forms.ChoiceField(choices=(), label='Institute Name', widget=forms.Select(attrs={'class':'selector'}) )

    def __init__(self, *args, **kwargs):
        super(StudentCourseInstituteForm, self).__init__(*args, **kwargs)
        self.fields['course'].error_messages = {'required': 'Select a Course'} 
        self.fields['institute'].error_messages = {'required': 'Select a Institute'}
        choices = [(int(ic.institute_id), unicode(ic.institute_name)) for ic in Institute_Course.objects.all()]
        self.fields['institute'].choices = choices

    def clean_course(self):
        course = None
        try:
            course = self.cleaned_data['course']
            if course and course in [None, '', 'Select Course',]:
                raise Exception 
        except Exception as e:
            raise forms.ValidationError("Select a Course") 
         
        # your other validations go here.
        return course

    
    def clean_institute(self):
        institute = None
        try:
            institute = self.cleaned_data['institute']
            if institute and institute in [None, '', 'Select Institute',]:
                raise Exception
        except Exception as e:
            raise forms.ValidationError("Select a Institute") 

         
        # your other validations go here.
        return institute



class StudentDocumentForm(forms.Form):
    ''' 
       This form view details which institute is accepting which identities. 
       with identitiy proof - StudentDocument File upload  
    '''

    error_messages_doc = {
        'acourse': ("Select a Course"),
        'ainstitute_course': ("Select a Institute"),
        'ainstitute_identity': ("Select an Identity Proof"),
        'aproofnumber': ("Enter a valid ID Proof number"), 
        'adocfile': ("Upload a file in .jpg, .jpeg, .png, .gif format"),
        'adocfilesize': ("Please keep filesize under 2.5MB"),
    }

    course = forms.ModelChoiceField(queryset=Course.objects.all(), label='Course', empty_label="Select Course")
    institute = forms.ChoiceField(choices=(), label='Institute Name', widget=forms.Select(attrs={'class':'selector'}) )
    identity = forms.ModelChoiceField(queryset=Institute_Identity.objects.all(),label='Identity Proof',empty_label="Select Identity Proof")
    proofnumber = forms.CharField(max_length=100, label='Enter Proof ID')
    docfile = forms.FileField(label='Select a file')
    filename = forms.CharField(widget=forms.HiddenInput())
    
    class Meta:
        model = Student_Document
        fields = ['course', 'institute', 'identity', 'proofnumber', 'docfile', 'filename']
 
    def __init__(self, *args, **kwargs):
        super(StudentDocumentForm, self).__init__(*args, **kwargs)
        self.content_types = ['image/jpg', 'image/jpeg' , 'image/png' , 'image/gif' ]
        self.max_upload_size = 2621440 # 1024 # 2.5MB 
        self.fields['filename'].initial = 'None.png'
        self.fields['course'].error_messages = {'required': 'Select a Course'} 
        self.fields['institute'].error_messages = {'required': 'Select a Institute'}
        self.fields['identity'].error_messages = {'required': 'Select an Identity Proof'}
        self.fields['proofnumber'].error_messages = {'required': 'Enter a valid ID Proof number'}
        self.fields['docfile'].error_messages = {'required': 'Choose a file to upload'}
        choices = [(int(ic.institute_id), unicode(ic.institute_name)) for ic in Institute_Course.objects.all()]
        self.fields['institute'].choices = choices
 
    def clean_docfile(self):
        docfile = self.cleaned_data['docfile']   

        try:
            content_type = docfile.content_type
            if (content_type in self.content_types): 
                if docfile.size > self.max_upload_size: # 
                    raise forms.ValidationError(self.error_messages_doc['adocfilesize'])
            else:
                raise forms.ValidationError(self.error_messages_doc['adocfile'])
             
        except Exception as e:
            raise forms.ValidationError(self.error_messages_doc['adocfile']) 

        
        # your other validations go here.
        return docfile


    def clean_course(self):
        course = None
        try:
            course = self.cleaned_data['course']
            if course and course in [None, '', 'Select Course',]:
                raise forms.ValidationError("Select a Course")
        except Exception as e:
            raise forms.ValidationError("Select a Course") 

         
        # your other validations go here.
        return course

    
    def clean_institute(self):
        institute = None
        try:
            institute = self.cleaned_data['institute']
            if institute and institute in [None, '', 'Select Institute',]:
                raise forms.ValidationError("Select a Institute")
        except Exception as e:
            raise forms.ValidationError("Select a Institute") 

         
        # your other validations go here.
        return institute

    
    def clean_identity(self):
        identity = None
        try:
            identity = self.cleaned_data['identity']
            if identity and identity in [None, '', 'Select Identity Proof',]:
                raise forms.ValidationError("Select an Identity Proof")
        except Exception as e:
            raise forms.ValidationError("Select an Identity Proof") 

         
        # your other validations go here.
        return identity
     

    def clean_proofnumber(self):
        proofnumber = None
        try:
            proofnumber = self.cleaned_data['proofnumber']
            if proofnumber:
                if len(proofnumber) == 0 :
                   raise forms.ValidationError("Enter a valid ID Proof number")
                valid = re.match('^[\w-]+$', proofnumber) is not None
                if valid is False:
                   raise forms.ValidationError("Enter a valid ID Proof number")
                 
        except Exception as e:
            raise forms.ValidationError("Enter a valid ID Proof number") 

         
        # your other validations go here.
        return proofnumber


