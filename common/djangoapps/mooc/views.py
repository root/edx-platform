"""
Mooc Views
"""
import os
import datetime
import json
import logging
import random
import re
import string       # pylint: disable=W0402
import urllib
import uuid
import time
import numpy as np
import sys
from collections import defaultdict
from pytz import UTC
import zipfile
import mooc.signals
import mooc.sync_backup
from django.conf import settings
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User,Group,AnonymousUser
from mooc.models import Institute_Registration, State, City, Institute_Accreditation, Accreditation, Institute_Designation, Role, Institute_Status, Student_Institute, Institute_Course, Identity, Institute_Identity, Student_Identity,Course_Registration
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import password_reset_confirm
# from django.contrib.sessions.models import Session
from django.core.cache import cache
from django.core.context_processors import csrf
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.core.validators import validate_email, validate_slug, ValidationError
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError, transaction
from django.http import (HttpResponse, HttpResponseBadRequest, HttpResponseForbidden,
                         Http404)
from mooc.forms import UploadFileForm

from django.shortcuts import redirect, render_to_response
from django_future.csrf import ensure_csrf_cookie
from django.utils.http import cookie_date, base36_to_int
from django.utils.translation import ugettext as _
from django.views.decorators.http import require_POST, require_GET
from django.contrib.admin.views.decorators import staff_member_required

from ratelimitbackend.exceptions import RateLimitException

from edxmako.shortcuts import render_to_response, render_to_string

sys.path.insert(0, '/edx/app/edxapp/edx-platform/cms/djangoapps/')

from course_creators.models import CourseCreator

from course_modes.models import CourseMode
from student.models import (
    Registration, UserProfile, PendingNameChange,
    PendingEmailChange, CourseEnrollment, unique_id_for_user,
    CourseEnrollmentAllowed, UserStanding, LoginFailures
)
from mooc.models import Person, Institute_Registration, Central_Coordinator

from student.forms import PasswordResetFormNoActive

from verify_student.models import SoftwareSecurePhotoVerification, MidcourseReverificationWindow
from certificates.models import CertificateStatuses, certificate_status_for_student

from xmodule.course_module import CourseDescriptor
from xmodule.modulestore.exceptions import ItemNotFoundError
from xmodule.modulestore.django import modulestore
from xmodule.modulestore import MONGO_MODULESTORE_TYPE

from collections import namedtuple

from courseware.courses import get_courses, sort_by_announcement
from courseware.access import has_access

from external_auth.models import ExternalAuthMap
import external_auth.views

from bulk_email.models import Optout, CourseAuthorization
import shoppingcart

import track.views

from dogapi import dog_stats_api
from pytz import UTC

from util.json_request import JsonResponse

from microsite_configuration.middleware import MicrositeConfiguration

from util.password_policy_validators import (
    validate_password_length, validate_password_complexity,
    validate_password_dictionary
)

from django.core.management import call_command
from django.db.models import Q
import commands
from student.models import *
from mooc.models import *
import ast
import hashlib
from django.utils.encoding import smart_str, force_unicode
from django.core.servers.basehttp import FileWrapper


import time
from datetime import datetime as etime
import os
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response as render_to_response_alias
from django.template import RequestContext
from mooc.forms import StudentDocumentForm, LocalAdminEmailForm
from mooc.models import Student_Document, Admin_Institute
from mooc.models import Institute_Status
from student.views import get_course_enrollment_pairs
from student.views import reverification_info

from mooc.forms import StudentCourseInstituteForm, StudentProofIdForm

#from django.contrib.auth.models import User
#from mooc.models import Person
#from student.models import UserProfile 


log = logging.getLogger("edx.student")
AUDIT_LOG = logging.getLogger("audit")

Article = namedtuple('Article', 'title url author image deck publication publish_date')
ReverifyInfo = namedtuple('ReverifyInfo', 'course_id course_name course_number date status display')  # pylint: disable=C0103

def csrf_token(context):
    """A csrf token that can be included in a form."""
    csrf_token = context.get('csrf_token', '')
    if csrf_token == 'NOTPROVIDED':
        return ''
    return (u'<div style="display:none"><input type="hidden"'
            ' name="csrfmiddlewaretoken" value="%s" /></div>' % (csrf_token))

class AccountValidationError(Exception):
    def __init__(self, message, field):
        super(AccountValidationError, self).__init__(message)
        self.field = field

def rpHash(person): 
    hash = 5381 
  
    value = person.upper() 
    for caracter in value: 
        hash = (( np.left_shift(hash, 5) + hash) + ord(caracter)) 
    hash = np.int32(hash)     
    return hash    

def nav_institute_register(request, extra_context=None):
    """
    This view will display the institute registration form
    """
    #if request.user.is_authenticated():
    #   return redirect(reverse('dashboard'))
    #if settings.FEATURES.get('AUTH_USE_MIT_CERTIFICATES_IMMEDIATE_SIGNUP'):
        # Redirect to branding to process their certificate if SSL is enabled
        # and registration is disabled.
    #    return redirect(reverse('root'))

    context = {
        'course_id': request.GET.get('course_id'),
        'enrollment_action': request.GET.get('enrollment_action'),
        'platform_name': MicrositeConfiguration.get_microsite_configuration_value(
            'platform_name',
            settings.PLATFORM_NAME
        ),
    }
    if extra_context is not None:
        context.update(extra_context)

    #if context.get("extauth_domain", '').startswith(external_auth.views.SHIBBOLETH_DOMAIN_PREFIX):
    #      return render_to_response('register-shib.html', context)
    return render_to_response('institute_register.html', context)

def GenerateUsername(email):

    

    try:
        User.objects.get(username=email)
	email += str(random.randint(0,1000))
        return GenerateUsername(email)
    except User.DoesNotExist:
        return email;

@ensure_csrf_cookie
def institute_register_redirect(request):
    """
    After institute registration, user is redirected to institute_register_redirect.html
    """
    return render_to_response("institute_register_redirect.html")

@ensure_csrf_cookie
def register_redirect(request,post_override=None):
    """
    After person registration, user is redirected to register_redirect.html
    """
    user = request.user
    return render_to_response("register_redirect.html",{'email': user.email})

@ensure_csrf_cookie
def register_institute(request, post_override=None):

    """
    JSON call to create new institute.
    """
    js = {'success': False}
    post_vars = post_override if post_override else request.POST
    extra_fields = getattr(settings, 'REGISTRATION_EXTRA_FIELDS', {})

    
    for a in ['name', 'state', 'city']:
        if a not in post_vars:
            js['value'] = _("Error (401 {field}). E-mail us.").format(field=a)
            js['field'] = a
            return JsonResponse(js, status=400)


    required_post_vars = ['name', 'state', 'city', 'pincode', 'address', 'website', 'headName', 'headEmail', 'headMobile', 'headCountry_code', 'rccName', 'rccEmail', 'rccMobile', 'rccCountry_code', 'accreditation', 'studentIdentity']


    for field_name in required_post_vars:
        if field_name in ('state', 'city', 'accreditation', 'studentIdentity'):
           min_length = 1
        elif field_name in ('address'):
            min_length = 5
        elif field_name in ('rccMobile','headMobile'):
            min_length = 10
        else:
           min_length = 2

        if len(post_vars[field_name]) < min_length:
            error_str = {
                'name': _('Name must be minimum of two characters long'),
                'state': _('A state is required'),
                'accreditation': _('Accreditation is required'),
                'studentIdentity': _('A Student Identity is required'),
                'address': _('Your address is required'),
                'city': _('A city is required'),
		        'pincode' : _('Your Pincode is required'),
                'website' : _('Your website is required'),
                'headName' : _('Head Name must be minimum of two characters long'),
                'headEmail' : _('A properly formatted e-mail is required'),
                'headMobile' : _('Head Mobile must be of 10 digits'),
                'headCountry_code' : _('Country Code is required'),
                'rccName' : _('RCC Name must be minimum of two characters long'),
                'rccEmail' : _('A properly formatted e-mail is required'),
                'rccMobile' : _('RCC Mobile must be of 10 digits'),
                'rccCountry_code' : _('Country Code is required'),
                'honor_code': _('Agreeing to the Honor Code is required'),
                'terms_of_service': _('Accepting Terms of Service is required')
            }
            js['value'] = error_str[field_name]
            js['field'] = field_name
            return JsonResponse(js, status=400)
    try:
        validate_email(post_vars['headEmail'])
    except ValidationError:
        js['value'] = _("Valid e-mail is required.").format(field=a)
        js['field'] = 'email'
        return JsonResponse(js, status=400)

    try:
        validate_email(post_vars['rccEmail'])
    except ValidationError:
        js['value'] = _("Valid e-mail is required.").format(field=a)
        js['field'] = 'email'
        return JsonResponse(js, status=400)



    if extra_fields.get('honor_code', 'required') == 'required' and \
            post_vars.get('honor_code', 'false') != u'true':
        js['value'] = _("To enroll, you must follow the honor code.").format(field=a)
        js['field'] = 'honor_code'
        return JsonResponse(js, status=400)


    if extra_fields.get('terms_of_service', 'required') == 'required' and \
            post_vars.get('terms_of_service', 'false') != u'true':
        js['value'] = _("To enroll, you must accept terms of service.").format(field=a)
        js['field'] = 'terms_of_service'
        return JsonResponse(js, status=400)
        
    if str(rpHash(post_vars['captcha'])) != str(post_vars['captchaHash']): 
        js['value'] = _("Enter Correct Captcha Value.").format(field=a)
        js['field'] = 'captcha'       
        return JsonResponse(js, status=400)

    
    status=Institute_Status.objects.filter(name="Pending")[0].id 

    institute = Institute_Registration( name=post_vars['name'], state_id=post_vars['state'], city_id=post_vars['city'], pincode=post_vars['pincode'], status_id=status, is_parent=False, address=post_vars['address'], website=post_vars['website'])


    if post_vars['headEmail'] == post_vars['rccEmail']:
            js['value'] = _("Please provide different emails for Head and Coordinator").format(email=post_vars['headEmail'])
            js['field'] = 'email'
            return JsonResponse(js,status=400)


    if len(User.objects.filter(email=str(post_vars['headEmail']))) > 0:
            js = {'success': False}
            js['value'] = _("An account with the Email '{email}' already exists.").format(email=post_vars['headEmail'])
            js['field'] = 'email'
            return JsonResponse(js,status=400)

    if len(User.objects.filter(email=str(post_vars['rccEmail']))) > 0:
            js = {'success': False}
            js['value'] = _("An account with the Email '{email}' already exists.").format(email=post_vars['rccEmail'])
            js['field'] = 'email'
            return JsonResponse(js,status=400)

    #regular expression for validating Institute Name, Head Name and Institute Coordinator Name
    regEx_name = re.compile("^[a-zA-Z]+[a-zA-Z\\. ]+$")

    if not regEx_name.match(post_vars['name']):
        js['value'] = _("Institute name must contain only alpabets.").format(name=post_vars['name'])
        js['field'] = 'name'
        return JsonResponse(js,status=400)

    if not regEx_name.match(post_vars['headName']):
        js['value'] = _("Head Name must contain only alpabets.").format(headName=post_vars['headName'])
        js['field'] = 'headName'
        return JsonResponse(js,status=400)

    if not regEx_name.match(post_vars['rccName']):
        js['value'] = _("Institute Coordinator name must contain only alphabets.").format(rccName=post_vars['rccName'])
        js['field'] = 'rccName'
        return JsonResponse(js,status=400)

    #regular expression for validating website
    regEx_website = re.compile("^(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$") 

    if not regEx_website.match(post_vars['website']):
        js['value'] = _("Enter a valid Website.").format(website=post_vars['website'])
        js['field'] = 'website'
        return JsonResponse(js,status=400)

    #regular expression for validating PinCode
    regEx_number = re.compile("^[1-9][0-9]*$") 

    if not regEx_number.match(post_vars['pincode']):
        js['value'] = _("Pin Code should not start with zero.").format(pincode=post_vars['pincode'])
        js['field'] = 'pincode'
        return JsonResponse(js,status=400) 

    #regular expression for validating Mobile Number
    regEx_contact = re.compile("^[1-9][0-9]*$") 

    if not regEx_contact.match(post_vars['headMobile']):
        js['value'] = _("Enter 10 digit Contact Number.").format(headMobile=post_vars['headMobile'])
        js['field'] = 'headMobile'
        return JsonResponse(js,status=400)

    if not regEx_contact.match(post_vars['rccMobile']):
        js['value'] = _("Enter 10 digit Contact Number.").format(rccMobile=post_vars['rccMobile'])
        js['field'] = 'rccMobile'
        return JsonResponse(js,status=400)   

    #regular expression for validating Country Code
    regEx_code = re.compile("^\+[1-9][0-9]*$") 

    if not regEx_code.match(post_vars['headCountry_code']):
        js['value'] = _("Enter a valid Country Code.").format(mobile=post_vars['mobile'])
        js['field'] = 'headCountry_code'
        return JsonResponse(js,status=400)  

    if not regEx_code.match(post_vars['rccCountry_code']):
        js['value'] = _("Enter a valid Country Code.").format(mobile=post_vars['mobile'])
        js['field'] = 'rccCountry_code'
        return JsonResponse(js,status=400) 
      


    try:
        institute.save()
    except IntegrityError as e:
        js = {'success': False}
        
        if len(Institute_Registration.objects.filter(name=post_vars['name'])) > 0:
            js['value'] = _("An Institute with the name '{name}' already exists.").format(name=post_vars['name'])
            js['field'] = 'name'
            return JsonResponse(js,status=400)
        
	
    insti_id= institute.id

    accreditation = request.POST.getlist('accreditation')

    for index in accreditation:
    	acc = Institute_Accreditation(accreditation_id=index , institute_id=insti_id)
    	acc.save()


    headUsername = post_vars['headEmail'].split('@')
    headUsername = GenerateUsername(headUsername[0])

    headPass = uuid.uuid4().hex[0:10]
    
    user = User(username=headUsername,
                email=post_vars['headEmail'],
                is_active=False)
    user.set_password(headPass)
   
    try:
        user.save()
        head_user_object = user
    except IntegrityError as e:
        js = {'success': False}
        # Figure out the cause of the integrity error
        if len(User.objects.filter(email=post_vars['headEmail'])) > 0:
            js['value'] = _("An account with the Email '{email}' already exists.").format(email=post_vars['headEmail'])
            js['field'] = 'email'
            return JsonResponse(js,status=400)

    profile = UserProfile(user=user)
    profile.name = post_vars['headName']
    profile.year_of_birth = None
    person = Person(user=user)
   
    person.mobile = post_vars.get('headMobile')
    person.country_code = post_vars.get('headCountry_code')
    person.save()
       
    try:
        profile.save()
    except Exception:
        log.exception("UserProfile creation failed for user {id}.".format(id=user.id))


    head_role_id = Role.objects.filter(name="Institute Head")[0].id


    designation = Institute_Designation(user=user, institute_id=insti_id, role_id=head_role_id, is_approved=False)
    designation.save()




    rccUsername = post_vars['rccEmail'].split('@')
    rccUsername = GenerateUsername(rccUsername[0])

    rccPass = uuid.uuid4().hex[0:10]
    
    user = User(username=rccUsername,
                email=post_vars['rccEmail'],
                is_active=False)
    user.set_password(rccPass)


 
   
   
    try:
        user.save()
        rcc_user_object = user
    except IntegrityError as e:
        js = {'success': False}
        # Figure out the cause of the integrity error
        if len(User.objects.filter(email=post_vars['rccEmail'])) > 0:
            js['value'] = _("An account with the Email '{email}' already exists.").format(email=post_vars['rccEmail'])
            js['field'] = 'email'
            return JsonResponse(js,status=400)

    profile = UserProfile(user=user)
    profile.name = post_vars['rccName']
    profile.year_of_birth = None
    person = Person(user=user)
   
    person.mobile = post_vars.get('rccMobile')
    person.country_code = post_vars.get('rccCountry_code')
    person.save()
       
    try:
        profile.save()
    except Exception:
        log.exception("UserProfile creation failed for user {id}.".format(id=user.id))

    studentIdentity = request.POST.getlist('studentIdentity')

    for index in studentIdentity:
        stdIdentity = Institute_Identity(identity_id=index , institute_id=insti_id)
        stdIdentity.save()


    ic_role_id = Role.objects.filter(name="Institute Coordinator")[0].id
    designation = Institute_Designation(user=user, institute_id=insti_id, role_id=ic_role_id, is_approved=False)
    designation.save()

    ## Inserting Identity value from institute registration to Identity table #
    #identity_name = post_vars.get('studentIdentity')
    #student_identity = Identity(name=identity_name)
    #student_identity.save()
    
    
    ## Inserting Institute Identity details in Institute_Identity
    #institute_id = Institute_Registration.objects.filter(name=post_vars.get('name'))[0].id
    #identity_id = Identity.objects.filter(name=identity_name)[0].id
    #institute_identity = Institute_Identity(institute_id=institute_id, identity_id=identity_id)
    #institute_identity.save() 
    

    context = {'name': "test",}

    # composes thank you email
    subject = render_to_string('emails/thankyou_email_subject.txt',context)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    message = render_to_string('emails/thankyou_email_body.txt',context)

    # cc mail to Central_Coordinator
    cc_subject = render_to_string('emails/enroll_institute_subject_cc.txt',context)
    # Email subject *must not* contain newlines
    cc_subject = ''.join(cc_subject.splitlines())
    cc_message = render_to_string('emails/enroll_institute_body_cc.txt',context)

    try:
        cc_user = User.objects.filter(email='iitbombayxcc@cse.iitb.ac.in',is_active=True)[0]
        Central_Coordinator_object = Central_Coordinator.objects.filter(user=cc_user, is_approved=1)[0] 
    except Exception:
        log.exception("Central_Coordinator object exception.")


    # don't send email if we are doing load testing or random user generation for some reason
    if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
        from_address = MicrositeConfiguration.get_microsite_configuration_value(
            'email_from_address',
            settings.DEFAULT_FROM_EMAIL
        )
        try:
            if settings.FEATURES.get('REROUTE_ACTIVATION_EMAIL'):
                dest_addr = settings.FEATURES['REROUTE_ACTIVATION_EMAIL']
                message = ("Thank you for mail %s (%s):\n" % (head_user_object, head_user_object.email) +
                           '-' * 80 + '\n\n' + message)
                send_mail(subject, message, from_address, [dest_addr], fail_silently=False)
            else:
                _res = head_user_object.email_user(subject, message, from_address)
                _res1 = rcc_user_object.email_user(subject, message, from_address)
                _res2 = Central_Coordinator_object.user.email_user(cc_subject, cc_message, from_address)
                
        except:
            log.warning('Unable to send thank you email to user', exc_info=True)
            js['value'] = _('Could not send thank you e-mail.')
            # What is the correct status code to use here? I think it's 500, because
            # the problem is on the server's end -- but also, the account was created.
            # Seems like the core part of the request was successful.
            return JsonResponse(js, status=500)     

    return JsonResponse({'success': True,})



@login_required
def token(request):
    '''
    Return a token for the backend of annotations.
    It uses the course id to retrieve a variable that contains the secret
    token found in inheritance.py. It also contains information of when
    the token was issued. This will be stored with the user along with
    the id for identification purposes in the backend.
    '''
    course_id = request.GET.get("course_id")
    course = course_from_id(course_id)
    dtnow = datetime.datetime.now()
    dtutcnow = datetime.datetime.utcnow()
    delta = dtnow - dtutcnow
    newhour, newmin = divmod((delta.days * 24 * 60 * 60 + delta.seconds + 30) // 60, 60)
    newtime = "%s%+02d:%02d" % (dtnow.isoformat(), newhour, newmin)
    secret = course.annotation_token_secret
    custom_data = {"issuedAt": newtime, "consumerKey": secret, "userId": request.user.email, "ttl": 86400}
    newtoken = create_token(secret, custom_data)
    response = HttpResponse(newtoken, mimetype="text/plain")
    return response

@ensure_csrf_cookie
def member_institutions(request):
    """
    This view displays the data of IITBombayX members
    """
        
    data = Institute_Registration.objects.all()
    context={'member_institutions':data,
                }
    

    return render_to_response('iitbombayx-members.html', context)

@ensure_csrf_cookie
def how_it_works(request):
    
    return render_to_response('how-it-works.html')


@login_required
@ensure_csrf_cookie
def cc_centers_course_list(request,post_override=None):
    """
    This view will display the courses registered by institutes
    """

    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else:
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"

    if not rolename == "Central Coordinator":
        return render_to_response('static_templates/404.html')

    institute_course_object = Institute_Course.objects.all()
    insti_course=[]
    for inst_course_object in institute_course_object:
        insti = Institute_Registration.objects.filter(id=inst_course_object.institute_id)[0]
        course_n = Course.objects.filter(id=inst_course_object.course_id)[0]
        temp=[]
       
        temp.append(insti.name)
        temp.append(course_n.name)
        insti_course.append(temp)
        
    
    
    #temp = {}
    #data = []
    #for item in institute_course_object:
    #    data.append(list(Institute_Registration.objects.filter(id=item.institute_id).values_list('id','name')[0]))
    #temp['']  = data

    
    context={'institute_course_object':insti_course,
             'rolename': rolename,
                }
    
    return render_to_response('centers_course_list.html', context)


@login_required
@ensure_csrf_cookie
def cc_institute_details(request,post_override=None):
    """
    This view will display the details of all the registered institutes
    """
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"

    if not rolename == "Central Coordinator":
        return render_to_response('static_templates/404.html')

    institute_object = Institute_Registration.objects.all()


    context={'institute_object':institute_object,
             'rolename': rolename,
                }
    
    return render_to_response('institute_details.html', context)

@login_required
@ensure_csrf_cookie
def cc_institute_details_approve(request,post_override=None):
    """
    This view will display the details of all the approved institutes
    """
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"

    if not rolename == "Central Coordinator":
        return render_to_response('static_templates/404.html')

    
    institute_object = Institute_Registration.objects.filter(status=1) #status=1 indicates Status="Approved"    


    context={'institute_object':institute_object,
             'rolename': rolename,
                }
    
    return render_to_response('institute_details_approve.html', context)

@login_required
@ensure_csrf_cookie
def cc_institute_details_pending(request,post_override=None):
    """
    This view will display the details of all the pending institutes
    """
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"

    if not rolename == "Central Coordinator":
        return render_to_response('static_templates/404.html')

    institute_object = Institute_Registration.objects.filter(status=2) #status=2 indicates Status="Pending"


    context={'institute_object':institute_object,
             'rolename': rolename,
                }
    
    return render_to_response('institute_details_pending.html', context)

@login_required
@ensure_csrf_cookie
def cc_institute_details_reject(request,post_override=None):
    """
    This view will display the details of all the rejected institutes
    """
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"

    if not rolename == "Central Coordinator":
        return render_to_response('static_templates/404.html')

    institute_object = Institute_Registration.objects.filter(status=3) #status=3 indicates Status="Rejected"


    context={'institute_object':institute_object,
             'rolename': rolename,
                }
    
    return render_to_response('institute_details_reject.html', context)

@login_required   
def course_list(request):
    """
    This view will display the course list of institute coordinator
    """
   
    return render_to_response('ic_course_list.html')

@login_required
@ensure_csrf_cookie
def approve_students(request):
    """
    This view will allow institute coordinator to approve the students
    """

    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                institute = int( role[0].institute.id)	
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"
    try:
        Student_Institute_Objects = Student_Institute.objects.filter(institute_id=institute)
        
        context = { 'Student_Institute_Objects' : Student_Institute_Objects,
                    'rolename': rolename,
                    }
        #return render_to_response('ic_approve_students.html',context)
        return render_to_response('ic_approve_studentsCopy.html',context)  
    except:
        return render_to_response('static_templates/404.html')

@login_required
@ensure_csrf_cookie
def enrolled_courses(request):
    """
    This view will display the courses enrolled by institute coordinator 
    """
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                institute = int( role[0].institute.id)	
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"
    try:

        Institute_Course_Objects = Institute_Course.objects.filter(institute_id=institute)
        context = { 'rolename': rolename, 'Institute_Course_Objects' : Institute_Course_Objects,
                    }
 
        return render_to_response('ic_enrolled_courses.html',context)
    except:
        return render_to_response('static_templates/404.html')


@login_required
@ensure_csrf_cookie
def ic_courses(request):
    """
    This view will display the students enrolled by institute coordinator
    """
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"
    context = { 'rolename': rolename,
                }   
    return render_to_response('courseware/courses.html',context)

@login_required
@ensure_csrf_cookie
def enrolled_students(request):
    """
    This view will display the students enrolled by institute coordinator
    """
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"
    context = { 'rolename': rolename,
                }   
    return render_to_response('ic_enrolled_students.html',context)


def handle_uploaded_file(file, filename):
    ''' Handle file upload
    '''  
    with open(settings.STUDENT_DOCUMENT_ROOT+filename, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)
   


@login_required
@ensure_csrf_cookie
def student_courseinstitute(request):
    ''' 
    This view allows Student to select Institute in Student Dashboard / 
    where identitiy proof has beed submitted earlier
    '''
    if not request.user.is_authenticated:
        raise Http404
    
    user = request.user
    institute_id = None
    course_id = None
    status=Institute_Status.objects.filter(name="Pending")[0].id

    isSuccess = None
    formNonFieldErrors = []

    if request.method == 'POST':
        log.info("Before select") 
        form = StudentCourseInstituteForm(request.POST)
        log.info('?????????')
        log.info(form.errors)
        log.info(form.is_valid())
        log.info('?')
        log.info(request.POST) 
        log.info('?????????') 
                  
        if form.is_valid():
            courseEnrollmentRecord = CourseEnrollment()
            studentRegistrationRecord = Student_Institute()   
            try:
                log.info("#")
                log.info(form.cleaned_data['course'])
                log.info(form.cleaned_data['institute'])   
                log.info("#")
                
                institute_id = request.POST['institute'] 
                course_id = request.POST['course'] 


                course_name = Course.objects.filter(id=course_id)[0].name
                s_role_id = Role.objects.filter(name="Student")[0].id

                if CourseEnrollment.is_enrolled(user, course_name):
                    formNonFieldErrors.append("Course already Enrolled earlier")                 
                
                if Student_Institute.is_existcourse(user, institute_id, course_id):
                    formNonFieldErrors.append("Course at Institute already selected earlier") 

                if len(formNonFieldErrors) > 0:
                    raise Exception
                
                
                if not Student_Institute.is_exist(user, institute_id):  
                    request.session['course_idu'] = course_id
                    request.session['institute_idu'] = institute_id
                    return HttpResponseRedirect(reverse('student_proofid', args=(1,)))
                

                # save to CourseEnrollment 
                courseEnrollmentRecord = CourseEnrollment(user=user, course_id=course_name, is_active=1, mode='honor')
                courseEnrollmentRecord.save()

                # save to Course_Registration Student_Institute
                studentRegistrationRecord = Student_Institute(user=user, institute_id=institute_id,status_id=status, course_id=course_id)     
                studentRegistrationRecord.save()

                student_identity_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].identity_id
                identity_name = Identity.objects.filter(id=student_identity_id)[0].name

                insti_iden_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].id
                log.info(insti_iden_id)

                
                datetime_now = etime.now()
                log.info("############## Date time now")
                log.info(datetime_now.strftime("%Y-%m-%d %H:%M:%S"))
    
                #course_name = Course.objects.filter(id=course_id)[0].name
                #CourseEnrollment_object = CourseEnrollment.enroll(user,course_name,'honor')

                # composes Institute Selection Information Mail
                subject = render_to_string('emails/select_institute_subject.txt',{})
                # Email subject *must not* contain newlines
                subject = ''.join(subject.splitlines())
                message = render_to_string('emails/select_institute_body.txt',{})
    
                # don't send email if we are doing load testing or random user generation for some reason
                if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
                    from_address = MicrositeConfiguration.get_microsite_configuration_value(
                        'email_from_address',
                        settings.DEFAULT_FROM_EMAIL
                    )
                    try:
                        ic_role_id = Role.objects.filter(name="Institute Coordinator")[0].id
                        Institute_Designation_object = Institute_Designation.objects.filter(institute_id=institute_id, role_id=ic_role_id)[0]
                        _res = Institute_Designation_object.user.email_user(subject, message, from_address)

                    except:
                        log.warning('Unable to send email to institute coordinator', exc_info=True)
                        js['value'] = _('Could not send e-mail.')
                        # What is the correct status code to use here? I think it's 500, because
                        # the problem is on the server's end -- but also, the account was created.
                        # Seems like the core part of the request was successful.
                        formNonFieldErrors.append("Could not send e-mail")  
                        isSuccess = False
                        ## return JsonResponse(js, status=500)     

                if len(formNonFieldErrors) > 0 :
                    raise Exception
                   
                isSuccess = True 
            except Exception as e:
                log.info("####")
                
                if courseEnrollmentRecord and courseEnrollmentRecord.pk:
                    courseEnrollmentRecord.delete() 
                 
                if studentRegistrationRecord and studentRegistrationRecord.pk:
                     studentRegistrationRecord.delete()
                

                log.info(e)
                log.info("####")
                form = StudentCourseInstituteForm()  
                isSuccess = False
                               
            # Redirect to the document list after POST
            #return HttpResponseRedirect(reverse('dashboard'))
        else:  # not form.is_valid()
            log.info("uload else")
            form = StudentCourseInstituteForm()
            isSuccess = False
    else: 
        form = StudentCourseInstituteForm() # A empty, unbound form

    
    if isSuccess:
        return HttpResponseRedirect(reverse('dashboard'))

    context = {'actiontitle': 'Select the Course and Institute',
               'form': form,
               'formNonFieldErrors': formNonFieldErrors 
              }
    
    return render_to_response('student_courseinstitute.html', context)




@login_required
@ensure_csrf_cookie
def student_proofid(request, step=None):
    ''' 
    This view allows Student to select Institute in Student Dashboard / 
    along with identitsy proof.
    '''
    if not request.user.is_authenticated:
        raise Http404
    if not request.session.get('course_idu', False):
        raise Http404
    if not request.session.get('institute_idu', False):
        raise Http404
    if not step=='1':
        raise Http404
    
    user = request.user
    institute_id = None
    institute_id_name = None  
    course_id = None
    course_name = None
    status=None
    isSuccess = None
    formNonFieldErrors = [] 

    try:
        institute_id = request.session.get('institute_idu', False)
        course_id = request.session.get('course_idu', False)
        status=Institute_Status.objects.filter(name="Pending")[0].id

        institute_id_name = Institute_Registration.objects.filter(id=institute_id)[0].name
        course_name = Course.objects.filter(id=course_id)[0].name

    except Exception as e0:
        raise Http404 

    if request.method == 'POST':
        log.info("Before file upload") 
       
        form = StudentProofIdForm(request.POST, request.FILES, institute_idn=institute_id)   
        log.info('?????????')
        #log.info(form.errors)
        #log.info(form.is_valid())
        log.info('?')
        log.info(request.POST) 
        log.info(request.FILES) 
        log.info('?????????') 
                  
        if form.is_valid():
            #newdoc = Student_Document(docfile = request.FILES['docfile'])
            studentDoc = Student_Document()
            courseEnrollmentRecord = CourseEnrollment()
            studentRegistrationRecord = Student_Institute() 
             
            try:
                docfile = request.FILES['docfile']
                docfileString = str(os.path.basename(str(docfile)))
                docname = str(int(time.mktime(etime.now().timetuple()))) + docfileString
                form.cleaned_data['filename'] = docname 
                log.info("#")
                log.info(form.cleaned_data['filename'])
                log.info(form.cleaned_data['docfile'])
                log.info(form.cleaned_data['docfile'].content_type)
                log.info(form.cleaned_data['docfile'].size)   
                #log.info(form.cleaned_data['course'])   
                log.info("#")

                # set values to Student_document
                studentDoc.filename = form.cleaned_data['filename'] 
                studentDoc.proofnumber = form.cleaned_data["proofnumber"]
                studentDoc.institute_identity = Institute_Identity.objects.filter(id=form.cleaned_data["identity"])[0]
                studentDoc.user = user


                if CourseEnrollment.is_enrolled(user, course_name):
                    formNonFieldErrors.append("Course already Enrolled earlier") 
                    
                    
                if Student_Institute.is_existcourse(user, institute_id, course_id):
                    formNonFieldErrors.append("Course at Institute already selected earlier")
 
                if Student_Document.is_exist(studentDoc.user, studentDoc.institute_identity, studentDoc.proofnumber):
                    formNonFieldErrors.append("Identity Proof already selected earlier") 
                   
 

                if len(formNonFieldErrors) > 0 :
                    raise Exception

                # save to CourseEnrollment 
                courseEnrollmentRecord = CourseEnrollment(user=user, course_id=course_name, is_active=1, mode='honor')
                courseEnrollmentRecord.save()

                # save to Student_Institute
                studentRegistrationRecord = Student_Institute(user=user, institute_id=institute_id,status_id=status,course_id=course_id)
                studentRegistrationRecord.save()
 
                    
                # handle_uploaded_file   
                handle_uploaded_file(request.FILES['docfile'], str(studentDoc.filename)) 

                log.info("Request FILES :::")
                log.info(request.FILES)

                # save to Student_document 
                studentDoc.save()
                

                student_identity_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].identity_id
                identity_name = Identity.objects.filter(id=student_identity_id)[0].name


                insti_iden_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].id
                log.info(insti_iden_id)


                #student_identity = Student_Identity(institute_identity_id=insti_iden_id, number=number, user_id=user.id)
                #log.info("%%%%%%%%%%%%STUDENT _IDENTITY")
                #student_identity.save()

                datetime_now = etime.now()
                log.info("############## Date time now")
                log.info(datetime_now.strftime("%Y-%m-%d %H:%M:%S"))
    
                #course_name = Course.objects.filter(id=course_id)[0].name
                #CourseEnrollment_object = CourseEnrollment.enroll(user,course_name,'honor')

                # composes Institute Selection Information Mail
                subject = render_to_string('emails/select_institute_subject.txt',{})
                # Email subject *must not* contain newlines
                subject = ''.join(subject.splitlines())
                message = render_to_string('emails/select_institute_body.txt',{})
    
                # don't send email if we are doing load testing or random user generation for some reason
                if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
                    from_address = MicrositeConfiguration.get_microsite_configuration_value(
                        'email_from_address',
                        settings.DEFAULT_FROM_EMAIL
                    )
                    try:
                        ic_role_id = Role.objects.filter(name="Institute Coordinator")[0].id
                        Institute_Designation_object = Institute_Designation.objects.filter(institute_id=institute_id, role_id=ic_role_id)[0]
                        _res = Institute_Designation_object.user.email_user(subject, message, from_address)

                    except:
                        log.warning('Unable to send email to institute coordinator', exc_info=True)
                        js['value'] = _('Could not send e-mail.')
                        # What is the correct status code to use here? I think it's 500, because
                        # the problem is on the server's end -- but also, the account was created.
                        # Seems like the core part of the request was successful.
                        formNonFieldErrors.append("Could not send e-mail")  
                        isSuccess = False
                        ## return JsonResponse(js, status=500)     

                if len(formNonFieldErrors) > 0 :
                    raise Exception
                   
                isSuccess = True 
            except Exception as e:
                log.info("####")

                if courseEnrollmentRecord and courseEnrollmentRecord.pk:
                    courseEnrollmentRecord.delete() 
                    
                if studentRegistrationRecord and studentRegistrationRecord.pk:
                    studentRegistrationRecord.delete()
                
                if studentDoc and studentDoc.pk:
                    studentDoc.delete()

                log.info(e)
                log.info("####")
                isSuccess = False
                               
            # Redirect to the document list after POST
            #return HttpResponseRedirect(reverse('dashboard'))
        else:  # not form.is_valid()
            log.info("uload else")
            isSuccess = False
    else: 
        form = StudentProofIdForm(institute_idn=institute_id) # A empty, unbound form

    if isSuccess:
        return HttpResponseRedirect(reverse('dashboard'))

    context = {'actiontitle': 'Select the Identity Proof',
               'institute_id_name': institute_id_name,
               'course_id_name': course_name,
               'form': form,
               'formNonFieldErrors': formNonFieldErrors 
              }
    
    return render_to_response('student_identityproof.html', context) 

"""
@login_required
@ensure_csrf_cookie
def student_documentssez(request):
     
    ''' 
    This view allows Student to select Institute in Student Dashboard / 
    with identitiy proof, handle file upload
    '''
    if not request.user.is_authenticated:
        raise Http404
    
    user = request.user
    #post_vars = post_override if post_override else request.POST
    institute_id = None # post_vars['institute']
    course_id = None # post_vars['course']
    status=Institute_Status.objects.filter(name="Pending")[0].id

    isSuccess = None
    formNonFieldErrors = []

    if request.method == 'POST':
        log.info("Before file upload") 
        form = StudentDocumentForm(request.POST, request.FILES)
        log.info('?????????')
        #log.info(form.errors)
        #log.info(form.is_valid())
        log.info('?')
        log.info(request.POST) 
        log.info(request.FILES) 
        log.info('?????????') 
                  
        if form.is_valid():
            #newdoc = Student_Document(docfile = request.FILES['docfile'])
            studentDoc = Student_Document()
            courseEnrollmentRecord = CourseEnrollment()
            studentInstituteRecord = Student_Institute()   
            try:
                docfile = request.FILES['docfile']
                docfileString = str(os.path.basename(str(docfile)))
                docname = str(int(time.mktime(etime.now().timetuple()))) + docfileString
                form.cleaned_data['filename'] = docname 
                log.info("#")
                log.info(form.cleaned_data['filename'])
                log.info(form.cleaned_data['docfile'])
                log.info(form.cleaned_data['docfile'].content_type)
                log.info(form.cleaned_data['docfile'].size)   
                log.info(form.cleaned_data['course'])   
                log.info("#")

                # set valves to Student_document
                studentDoc.filename = form.cleaned_data['filename'] 
                studentDoc.proofnumber = form.cleaned_data["proofnumber"]
                studentDoc.institute_identity = form.cleaned_data["identity"]
                studentDoc.user = user

                institute_id = request.POST['institute'] 
                course_id = request.POST['course'] 


                course_name = Course.objects.filter(id=course_id)[0].name

                #if CourseEnrollment.is_enrolled(user, course_name):
                    #formNonFieldErrors.append("Course already Enrolled earlier") 
                    
                #if Student_Institute.is_exist(user, institute_id):
                    #formNonFieldErrors.append("Institute already selected earlier") 
                    
                #if Student_Document.is_exist(studentDoc.user, studentDoc.institute_identity, studentDoc.proofnumber):
                    #formNonFieldErrors.append("Identity Proof already selected earlier") 
                    
                if Course_Registration.is_exist(user, institute_id, course_id):
                    formNonFieldErrors.append("You are already registered for this course under this institute") 

                else:
                    # save to CourseEnrollment 
                    courseEnrollmentRecord = CourseEnrollment(user=user, course_id=course_name, is_active=1, mode='honor')
                    courseEnrollmentRecord.save()

                    # save to Student_Institute
                    if len(Student_Institute.objects.filter(user=user, institute_id=institute_id)) <= 0:

                        studentInstituteRecord = Student_Institute(user=user, institute_id=institute_id,status_id=status)
                        studentInstituteRecord.save() 
                
                    # save to Course Registration
                    courseRegistrationRecord = Course_Registration(user=user, institute_id=institute_id,status_id=status,course_id=course_id,role_id=4,is_approved=1)
                    courseRegistrationRecord.save() 
                    
                    # handle_uploaded_file   
                    handle_uploaded_file(request.FILES['docfile'], str(studentDoc.filename)) 

                    log.info("Request FILES :::")
                    log.info(request.FILES)

                    # save to Student_document 
                    studentDoc.save()
                

                if len(formNonFieldErrors) > 0 :
                    raise Exception
                student_identity_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].identity_id
                identity_name = Identity.objects.filter(id=student_identity_id)[0].name


                insti_iden_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].id
                log.info(insti_iden_id)


                #student_identity = Student_Identity(institute_identity_id=insti_iden_id, number=number, user_id=user.id)
                #log.info("%%%%%%%%%%%%STUDENT _IDENTITY")
                #student_identity.save()

                datetime_now = etime.now()
                log.info("############## Date time now")
                log.info(datetime_now.strftime("%Y-%m-%d %H:%M:%S"))
    
                #course_name = Course.objects.filter(id=course_id)[0].name
                #CourseEnrollment_object = CourseEnrollment.enroll(user,course_name,'honor')

                # composes Institute Selection Information Mail
                subject = render_to_string('emails/select_institute_subject.txt',{})
                # Email subject *must not* contain newlines
                subject = ''.join(subject.splitlines())
                message = render_to_string('emails/select_institute_body.txt',{})
    
                # don't send email if we are doing load testing or random user generation for some reason
                if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
                    from_address = MicrositeConfiguration.get_microsite_configuration_value(
                        'email_from_address',
                        settings.DEFAULT_FROM_EMAIL
                    )
                    try:
                        ic_role_id = Role.objects.filter(name="Institute Coordinator")[0].id
                        Institute_Designation_object = Institute_Designation.objects.filter(institute_id=institute_id, role_id=ic_role_id)[0]
                        _res = Institute_Designation_object.user.email_user(subject, message, from_address)

                    except:
                        log.warning('Unable to send email to institute coordinator', exc_info=True)
                        js['value'] = _('Could not send e-mail.')
                        # What is the correct status code to use here? I think it's 500, because
                        # the problem is on the server's end -- but also, the account was created.
                        # Seems like the core part of the request was successful.
                        formNonFieldErrors.append("Could not send e-mail")  
                        isSuccess = False
                        ## return JsonResponse(js, status=500)     

                if len(formNonFieldErrors) > 0 :
                    raise Exception
                   
                isSuccess = True 
            except Exception as e:
                log.info("####")

                if courseEnrollmentRecord and courseEnrollmentRecord.pk:
                    courseEnrollmentRecord.delete() 
                    
                if studentInstituteRecord and studentInstituteRecord.pk:
                    studentInstituteRecord.delete()

                if studentDoc and studentDoc.pk:
                    studentDoc.delete()

                log.info(e)
                log.info("####")
                isSuccess = False
                               
            # Redirect to the document list after POST
            #return HttpResponseRedirect(reverse('dashboard'))
        else:  # not form.is_valid()
            log.info("uload else")
            isSuccess = False
    else: 
        form = StudentDocumentForm() # A empty, unbound form

    # Load documents for the list page
    documents = Student_Document.objects.all()
    '''
    if isSuccess:

        student_identity_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].identity_id
        identity_name = Identity.objects.filter(id=student_identity_id)[0].name


        insti_iden_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].id
        log.info(insti_iden_id)


        #student_identity = Student_Identity(institute_identity_id=insti_iden_id, number=number, user_id=user.id)
        #log.info("%%%%%%%%%%%%STUDENT _IDENTITY")
        #student_identity.save()

        datetime_now = etime.now()
        log.info("############## Date time now")
        log.info(datetime_now.strftime("%Y-%m-%d %H:%M:%S"))
    
        #course_name = Course.objects.filter(id=course_id)[0].name
        #CourseEnrollment_object = CourseEnrollment.enroll(user,course_name,'honor')

        # composes Institute Selection Information Mail
        subject = render_to_string('emails/select_institute_subject.txt',{})
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        message = render_to_string('emails/select_institute_body.txt',{})
    
        # don't send email if we are doing load testing or random user generation for some reason
        if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
            from_address = MicrositeConfiguration.get_microsite_configuration_value(
                'email_from_address',
                settings.DEFAULT_FROM_EMAIL
            )
            try:
                ic_role_id = Role.objects.filter(name="Institute Coordinator")[0].id
                Institute_Designation_object = Institute_Designation.objects.filter(institute_id=institute_id, role_id=ic_role_id)[0]
                _res = Institute_Designation_object.user.email_user(subject, message, from_address)

            except:
                log.warning('Unable to send email to institute coordinator', exc_info=True)
                js['value'] = _('Could not send e-mail.')
                # What is the correct status code to use here? I think it's 500, because
                # the problem is on the server's end -- but also, the account was created.
                # Seems like the core part of the request was successful.
                isSuccess = False
                ## return JsonResponse(js, status=500)     
    '''
    if isSuccess:
        return HttpResponseRedirect(reverse('dashboard'))

    context = {'actiontitle': 'Select the Course and Institute',
               'documents': documents, 
               'form': form,
               'formNonFieldErrors': formNonFieldErrors 
              }
    
    return render_to_response('student_documents.html', context)
"""


@login_required
@ensure_csrf_cookie
def ins_selection(request, post_override=None):
    """ 
    This view allows Student to select Institute in Student Dashboard 
    """
    if not request.user.is_authenticated:
        raise Http404
    
    user = request.user
    post_vars = post_override if post_override else request.POST
    institute_id = post_vars['ins_id']
    course_id = post_vars['course_id']
    status=Institute_Status.objects.filter(name="Pending")[0].id
    
    try:    
        Student_Institute(user=user, institute_id=institute_id,status_id=status).save()    
    except IntegrityError:
        log.info("Integrity error")

    student_identity_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].identity_id
    identity_name = Identity.objects.filter(id=student_identity_id)[0].name

    number = post_vars['proof_id']

    
    log.info("Before file upload")
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
	#log.info(UploadFileForm())
	log.info(form.errors)
	log.info("Request POST::::")
        log.info(request.POST)
        log.info(request.POST['file_name'])
	log.info("Request FILES :::")
        log.info(request.FILES)
        if form.is_valid():
	    log.info("fist line file upload if")
            new_file = Student_Identity(file_name = request.FILES['file_name'])
            new_file.save()
        else: 
	    log.info("uload else")
    else:
        form = UploadFileForm()
	log.info("inside else")




    log.info("############## INSITUTE ID")
    log.info(institute_id)
    log.info("############## COURSE ID")
    log.info(course_id)
    log.info("############## USER")
    log.info(user)
    log.info("############## STATUS")
    log.info(status)
    log.info("############## IDENTITY NAME")
    log.info(identity_name)
    log.info("############## NUMBER")
    log.info(number)


    student_identity_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].identity_id
    identity_name = Identity.objects.filter(id=student_identity_id)[0].name


    insti_iden_id = Institute_Identity.objects.filter(institute_id=institute_id)[0].id
    log.info(insti_iden_id)


    student_identity = Student_Identity(institute_identity_id=insti_iden_id, number=number, user_id=user.id)
    log.info("%%%%%%%%%%%%STUDENT _IDENTITY")
    student_identity.save()

    datetime_now = datetime.now()
    log.info("############## Date time now")
    log.info(datetime_now.strftime("%Y-%m-%d %H:%M:%S"))
    
    course_name = Course.objects.filter(id=course_id)[0].name
    CourseEnrollment_object = CourseEnrollment.enroll(user,course_name,'honor')

    # composes Institute Selection Information Mail
    subject = render_to_string('emails/select_institute_subject.txt',{})
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    message = render_to_string('emails/select_institute_body.txt',{})
    
    # don't send email if we are doing load testing or random user generation for some reason
    if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
        from_address = MicrositeConfiguration.get_microsite_configuration_value(
            'email_from_address',
            settings.DEFAULT_FROM_EMAIL
        )
        try:
             ic_role_id = Role.objects.filter(name="Institute Coordinator")[0].id
             Institute_Designation_object = Institute_Designation.objects.filter(institute_id=institute_id, role_id=ic_role_id)[0]
             _res = Institute_Designation_object.user.email_user(subject, message, from_address)

        except:
            log.warning('Unable to send email to institute coordinator', exc_info=True)
            js['value'] = _('Could not send e-mail.')
            # What is the correct status code to use here? I think it's 500, because
            # the problem is on the server's end -- but also, the account was created.
            # Seems like the core part of the request was successful.
            return JsonResponse(js, status=500)     

    return HttpResponse(json.dumps({'success': True}))

@login_required
def update_institute(request):
    """
    This view will allow the student to select more than one institute
    """
    status=Institute_Status.objects.filter(name="Pending")[0].id
    Student_Institute_Objects = Student_Institute.objects.filter(status_id=status)

    context = { "Student_Institute_Objects" : Student_Institute_Objects, }
    return render_to_response('update_institute.html',context)

@login_required
def student_available_courses(request):
    """
    This view will display the courses of the institute for which the student is registered
    """
   
    return render_to_response('student_available_courses.html')


def ins_approve(request, post_override=None):
    """
    This view will allow the central coordinater to approve registered institutes
    """
    if not request.user.is_authenticated:
        raise Http404

    post_vars = post_override if post_override else request.POST
    institute_id = post_vars['ins_id']
    status = post_vars['ins_status']
    try:    
        Institute_object = Institute_Registration.objects.filter(id=institute_id)[0]
        status_id = int ( Institute_Status.objects.filter(name=status)[0].id )
        Institute_object.status_id = status_id
        Institute_object.save()
        #initialize(institute_id)    
    except:
        raise

    try:
        ic_role_id = Role.objects.filter(name="Institute Coordinator")[0].id    
        Institute_Designation_object = Institute_Designation.objects.filter(institute_id=institute_id, role_id=ic_role_id)[0]   
    except:
        raise
    
    try:
        Password = uuid.uuid4().hex[0:10]
        Institute_Designation_object.user.set_password(Password)
        Institute_Designation_object.user.is_active=1
        Institute_Designation_object.user.save()
    except:
        raise

    
    if status=="Approved" :
        context= { "Email" : Institute_Designation_object.user.email, "Password" : Password }
        # composes Approve Institute email
        subject = render_to_string('emails/approve_institute_email_subject.txt',context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        message = render_to_string('emails/approve_institute_email_body.txt',context)
    else :
        context= {}
        #composes Reject Institute email
        subject = render_to_string('emails/reject_institute_email_subject.txt',context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        message = render_to_string('emails/reject_institute_email_body.txt',context)


    # don't send email if we are doing load testing or random user generation for some reason
    if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
        from_address = MicrositeConfiguration.get_microsite_configuration_value(
            'email_from_address',
            settings.DEFAULT_FROM_EMAIL
        )
        try:
            _res = Institute_Designation_object.user.email_user(subject, message, from_address)

        except:
            log.warning('Unable to send Approve/Reject email to institute', exc_info=True)
            js['value'] = _('Could not send Approve/Reject e-mail.')
            # What is the correct status code to use here? I think it's 500, because
            # the problem is on the server's end -- but also, the account was created.
            # Seems like the core part of the request was successful.
            return JsonResponse(js, status=500)     

    return HttpResponse(json.dumps({'success': True}))

def fetch_ins_details(request, post_override=None):
    """
    This view will allow the central coordinater to view data of a particular institute.
    """
    if not request.user.is_authenticated:
        raise Http404


    post_vars = post_override if post_override else request.POST
    institute_id = post_vars['ins_id']

    try:
        institute_head_role_id=Role.objects.filter(name="Institute Head")[0].id
        institute_coordinator_role_id=Role.objects.filter(name="Institute Coordinator")[0].id
    except:
        raise

    insti = Institute_Registration.objects.filter(id=institute_id)[0]
    inner_list = []
    inner_list.append(insti)
    try:
        inner_list.append(Institute_Designation.objects.filter(institute_id = insti.id, role_id=institute_head_role_id)[0])
        inner_list.append(Institute_Designation.objects.filter(institute_id = insti.id, role_id=institute_coordinator_role_id)[0])
    except:
        raise

    js ={ 'success': True,
          'name':str(inner_list[0].name),
          'address': str(inner_list[0].address),
          'city': str(inner_list[0].city),
          'state': str(inner_list[0].state),
          'pincode': str(inner_list[0].pincode),
          'website': str(inner_list[0].website),
          'status': str(inner_list[0].status),
          'head_name': str(inner_list[1].user.profile.name),
          'head_email': str(inner_list[1].user.email),
          'head_contact': str(inner_list[1].user.person.mobile),
          'coordinator_name': str(inner_list[2].user.profile.name),
          'coordinator_email': str(inner_list[2].user.email),
          'coordinator_contact': str(inner_list[2].user.person.mobile)
        }

    return HttpResponse(json.dumps(js))


def fetch_student_details(request, post_override=None):
    '''
    This view will allow the central coordinater to view data of a particular student.
    '''
    if not request.user.is_authenticated:
        raise Http404

    post_vars = post_override if post_override else request.POST 
    student_id = post_vars['student_id']
    
    studentdata = dict()

    try:
        #aUser=User.objects.filter(id=student_id)[0]
        aStudentInstitute = Student_Institute.objects.filter(id=student_id)[0]
        auserid = aStudentInstitute.user_id
        aPerson = Person.objects.filter(user_id=auserid)[0]
        aProfile = UserProfile.objects.filter(user_id=auserid)[0]
        aStudentDocument = Student_Document.objects.filter(user_id=auserid)[0]
        
        studentdata['coursename'] = str(aStudentInstitute.course.name)
        studentdata['email'] = str(aPerson.user.email)
        studentdata['username'] = str(aPerson.user.username)
        studentdata['fullname'] = str(aProfile.name)
        studentdata['education'] = str([codes for codes in UserProfile.LEVEL_OF_EDUCATION_CHOICES if aProfile.level_of_education in codes][0][1]) or ''
        studentdata['gender'] = str([codes for codes in UserProfile.GENDER_CHOICES if aProfile.gender in codes][0][1]) or ''
        studentdata['mailing_address'] = str(aProfile.mailing_address)
        studentdata['birth_date'] = str(aPerson.birth_date)
        studentdata['contactnumber'] = ((aPerson.country_code) or '') + str(aPerson.mobile)

        studentdata['identityproof'] = str(aStudentDocument.institute_identity.identity.name)
        studentdata['proofid'] = str(aStudentDocument.proofnumber)
        studentdata['proofidfile'] = str(aStudentDocument.filename)

    except:
        raise
    
    js ={ 'success': True,
          'coursename': studentdata.get('coursename', ''),
          'email': studentdata.get('email', ''),
          'username': studentdata.get('username', ''),
          'fullname': studentdata.get('fullname', ''),
          'education_level': studentdata.get('education',''),
          'gender': studentdata.get('gender',''),
          'mailing_address': studentdata.get('mailing_address', '') ,
          'birth_date': studentdata.get('birth_date', '') ,
          'contactnumber': studentdata.get('contactnumber', ''),
          'identityproof': studentdata.get('identityproof', ''),
          'proofid': studentdata.get('proofid', ''),
          'proofidfile': studentdata.get('proofidfile', '#')
        }

    return HttpResponse(json.dumps(js))



def fetch_fac_details(request, post_override=None):
    """
    This view will allow the central course assistants to view data of a particular faculty.
    """
    if not request.user.is_authenticated:
        raise Http404


    post_vars = post_override if post_override else request.POST
    faculty_id = post_vars['faculty_id']

    try:
        local_faculty_role_id=Role.objects.filter(name="Local Faculty")[0].id
        institute_coordinator_role_id=Role.objects.filter(name="Institute Coordinator")[0].id       
        
    except:
        raise

    faculty = Faculty_Institute.objects.filter(id=faculty_id)[0]
    inner_list = []
    inner_list.append(faculty)
    try:
        inner_list.append(Course.objects.filter(id = faculty.course_id)[0])
        inner_list.append(Institute_Registration.objects.filter(id = faculty.institute_id)[0])
        inner_list.append(Institute_Designation.objects.filter(institute_id = faculty.institute_id, role_id=institute_coordinator_role_id)[0])
    except:
        raise

    js ={ 'success': True,
          'faculty_id':str(inner_list[0].id),
          'user_id': str(inner_list[0].user_id),
	  'course_id': str(inner_list[0].course_id),
          'course_name': str(inner_list[1].name),
          'Insti_name': str(inner_list[2].name),
          'website': str(inner_list[2].website),
          'status': str(inner_list[0].status),
          'coordinator_name': str(inner_list[3].user.profile.name),
          'coordinator_email': str(inner_list[3].user.email),
          'coordinator_contact': str(inner_list[3].user.person.mobile)


        }

    return HttpResponse(json.dumps(js))


def student_approve(request,post_override=None):
    """
    This view will allow the Institute coordinater to approve registered students
    """
    if not request.user.is_authenticated:
        raise Http404

    post_vars = post_override if post_override else request.POST
    student_id = post_vars['student_id']
    status = post_vars['student_status']

    try:
        Student_object = Student_Institute.objects.filter(id=student_id)[0]
        status_id = int ( Institute_Status.objects.filter(name=status)[0].id )
        Student_object.status_id = status_id
        Student_object.save() 
         
    except:
        raise

    context= { }
    if status=="Approved" :
        # composes Approve Student email
        subject = render_to_string('emails/approve_student_email_subject.txt',context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        message = render_to_string('emails/approve_student_email_body.txt',context)
    else :
        #composes Reject Student email
        subject = render_to_string('emails/reject_student_email_subject.txt',context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        message = render_to_string('emails/reject_student_email_body.txt',context)


    # don't send email if we are doing load testing or random user generation for some reason
    if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
        from_address = MicrositeConfiguration.get_microsite_configuration_value(
            'email_from_address',
            settings.DEFAULT_FROM_EMAIL
        )
        try:
            _res = Student_object.user.email_user(subject, message, from_address)

        except:
            log.warning('Unable to send Approve/Reject email to user', exc_info=True)
            js['value'] = _('Could not send Approve/Reject e-mail.')
            # What is the correct status code to use here? I think it's 500, because
            # the problem is on the server's end -- but also, the account was created.
            # Seems like the core part of the request was successful.
            return JsonResponse(js, status=500)     


    return HttpResponse(json.dumps({'success': True}))

@ensure_csrf_cookie
@login_required
def ic_configuration(request):
    """
    This view will display the fields required for script download by Institute Coordinator
    """
   
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                institute = int( role[0].institute.id)	
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"

    context = { 'rolename': rolename,
                }
    return render_to_response('ic_configuration.html',context)

@ensure_csrf_cookie
@login_required
def download_script(request):
    """
    This view will display will redirect to download script page for Institute Coordinator
    
   
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                institute = int( role[0].institute.id)	
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"

    context = { 'rolename': rolename,
                }
    """
    file_wrapper = FileWrapper(file("/edx/app/edxapp/edx-platform/common/djangoapps/mooc/IITBX-Local-Setup.zip",'rb')) 
    #file_mimetype = mimetypes.guess_type("./IITBX-Local-Setup.zip")
    response = HttpResponse(file_wrapper,content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename=%s' % smart_str("IITBX-Local-Setup.zip")
    response['Content-Length'] = os.stat("/edx/app/edxapp/edx-platform/common/djangoapps/mooc/IITBX-Local-Setup.zip").st_size
    response['X-Sendfile'] = smart_str("./")
    return response
    """return render_to_response('download-script.html',context)"""

@ensure_csrf_cookie
@login_required
def ic_configuration_action(request):

	platform_name=request.POST.get('platform_name')	
	site_name=request.POST.get('site_name')
	email=request.POST.get('email')

	filedetail=open("platformname.txt",'w')
	filedetail.write("Platform Name "+platform_name+"\n")
	filedetail.write("Site Name "+site_name+"\n")
	filedetail.write("Email "+Email+"\n")
	filedetail.close()


def helpsupport(request):
    """
    This view will redirect to the help and support page.
    """
    return render_to_response('helpsupport.html')

def central_coordinator(request):
    """
    This view will redirect to the help and support central_coordinator page.
    """
    return render_to_response('central_coordinator.html')

def site_supervisor(request):
    """
    This view will redirect to the help and support site_supervisor page.
    """
    return render_to_response('site_supervisor.html')

def central_course_creator(request):
    """
    This view will redirect to the help and support central_course_creator page.
    """
    return render_to_response('central_course_creator.html')

def central_course_assistant(request):
    """
    This view will redirect to the help and support central_course_assistant page.
    """
    return render_to_response('central_course_assistant.html')
def institute_coordinator(request):
    """
    This view will redirect to the help and support institute_coordinator page.
    """
    return render_to_response('institute_coordinator.html')
def institute_admin(request):
    """
    This view will redirect to the help and support  institute_admin page.
    """
    return render_to_response('institute_admin.html')

def institute_faculty(request):
    """
    This view will redirect to the help and support  institute_faculty page.
    """
    return render_to_response('institute_faculty.html')

def student(request):
    """
    This view will redirect to the help and support  student page.
    """
    return render_to_response('student.html')

def faq(request):
    """
    This view will redirect to the help and support central_coordinator faq page.
    """
    return render_to_response('faq.html')


def central_coordinator_faq(request):
    """
    This view will redirect to the help and support central_coordinator faq page.
    """
    return render_to_response('central_coordinator_faq.html')

def site_supervisor_faq(request):
    """
    This view will redirect to the help and support site_supervisor _faq page.
    """
    return render_to_response('site_supervisor_faq.html')

def central_course_creator_faq(request):
    """
    This view will redirect to the help and support central_course_creator_faq page.
    """
    return render_to_response('central_course_creator_faq.html')

def central_course_assistant_faq(request):
    """
    This view will redirect to the help and support central_course_assistant _faq page.
    """
    return render_to_response('central_course_assistant_faq.html')
def institute_coordinator_faq(request):
    """
    This view will redirect to the help and support institute_coordinator_faq page.
    """
    return render_to_response('institute_coordinator_faq.html')
def institute_admin_faq(request):
    """
    This view will redirect to the help and support  institute_admin_faq page.
    """
    return render_to_response('institute_admin_faq.html')

def institute_faculty_faq(request):
    """
    This view will redirect to the help and support  institute_faculty_faq page.
    """
    return render_to_response('institute_faculty_faq.html')

def student_faq(request):
    """
    This view will redirect to the help and support  student_faq page.
    """
    return render_to_response('student_faq.html')
def about(request):
    """
    This view will redirect to the help and support about page.
    """
    return render_to_response('about.html')
def team(request):
    """
    This view will redirect to the team members page.
    """
    return render_to_response('team.html')
def about(request):
    """
    This view will redirect to the team members page.
    """
    return render_to_response('about.html')

def contact(request):
    """
    This view will redirect to the team members page.
    """
    return render_to_response('contact.html')


@login_required
@ensure_csrf_cookie
def local_admin(request,post_override=None):
    """
    This view will display the details of all the requested/approved local admin for institute
    """
    user = request.user
    rolename=None
    ic_institute_id=None
    lad_status_id=None
    lad_role_id=None 
    lad_user_id=None
    Institute_Local_Admin_Objects=None 
    isSuccess = None
    formNonFieldErrors = [] 
    email_id = None
    localadminRecord = None

    try:
        if user.is_superuser :
            rolename="Admin"
        else:
            role=Institute_Designation.objects.filter(user=user)
            ic_institute_id = role[0].institute.id  
   	    role_names=Role.objects.filter(name=role[0].role)
   	    rolename=str(role_names[0])
   	    log.info("*************")
   	    log.info(rolename)
   	    lad_role_id = Role.objects.filter(name=settings.LOGIN_ROLES.get('ROLE_LAD'))[0].id 
            lad_status_id = Institute_Status.objects.filter(name='Pending')[0].id 
   	    Institute_Local_Admin_Objects = Admin_Institute.objects.filter(institute_id=ic_institute_id)    
    except:
    	    rolename=None
   	   	   	   	   
   	   	   	   	   
    if not rolename == settings.LOGIN_ROLES.get('ROLE_IC'): # "Institute Coordinator":
        return render_to_response('static_templates/404.html')
    	    
    if rolename==None:
        return render_to_response('static_templates/404.html') 	    
    	    
    if request.method == 'POST':
        log.info("Before POST") 
        form = LocalAdminEmailForm(request.POST)
        log.info('??-??-??-')
        log.info(request.POST) 
        log.info('??-??-??-')
        if form.is_valid():
            try:
                email_id = form.cleaned_data["emailid"]
                usera = User.objects.filter(email=email_id)
                if not len(usera) == 1:
                    formNonFieldErrors.append("Enter a valid e-mail address") 
                elif not usera[0].is_active:
                    formNonFieldErrors.append("User should activate account first.") 

                if len(formNonFieldErrors) > 0 :
                    raise Exception

                lad_user_id = usera[0].id 

                log.info('??-??-??-')
                log.info(lad_user_id)
                log.info(ic_institute_id)
                #log.info(lad_role_id)
                log.info(lad_status_id) 
                log.info('??-??-??-')   

                if Admin_Institute.is_exist(lad_user_id, ic_institute_id):
                    formNonFieldErrors.append("User already selected earlier")

                if len(formNonFieldErrors) > 0 :
                    raise Exception

                # save to Admin_Institute for Local Admin
                localadminRecord = Admin_Institute(user_id=lad_user_id, institute_id=ic_institute_id,status_id=lad_status_id)
                localadminRecord.save()

                isSuccess = True 
                
            except Exception as e:
                log.info("####")
                log.info(e)
                if localadminRecord and localadminRecord.pk:
                    localadminRecord.delete()

                 
                isSuccess = False 
        else:  # not form.is_valid()
            log.info("uload else")
            isSuccess = False
    else: 
        form = LocalAdminEmailForm() # A empty, unbound form
    
    
    if isSuccess:
        return HttpResponseRedirect(reverse('local_admin'))

    context={'Institute_Local_Admin_Objects':Institute_Local_Admin_Objects,
    	    'rolename': rolename,
            'form': form,  
            'formNonFieldErrors': formNonFieldErrors 
    }
    
    return render_to_response('nav_local_admin.html', context)


@login_required
@ensure_csrf_cookie
def approve_admininstitute(request):
    """
    This view will allow central coordinator to approve the local institute admin
    """

    user = request.user
    rolename=None
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        rolename=None
        
    if not rolename == settings.LOGIN_ROLES.get('ROLE_CC'): # "Central Coordinator ":
        return render_to_response('static_templates/404.html')

    try:
        Admin_Institute_Objects = Admin_Institute.objects.all()
        
        context = { 'Admin_Institute_Objects' : Admin_Institute_Objects,
                    'rolename': rolename,
                    }
        
        return render_to_response('cc_approve_admininstitute.html',context)  
    except:
        return render_to_response('static_templates/404.html')


def fetch_admininstitute_details(request, post_override=None):
    '''
    This view will allow the central coordinater to view data of a particular admininstitute.
    '''
    if not request.user.is_authenticated:
        raise Http404

    post_vars = post_override if post_override else request.POST 
    admininstitute_id = post_vars['admininstitute_id']
    
    admininstitutedata = dict()

    try:
        
        aAdminInstitute = Admin_Institute.objects.filter(id=admininstitute_id)[0]
        auserid = aAdminInstitute.user_id
        aPerson = Person.objects.filter(user_id=auserid)[0]
        aProfile = UserProfile.objects.filter(user_id=auserid)[0]
        
        admininstitutedata['institutename'] = str(aAdminInstitute.institute.name)  
        admininstitutedata['email'] = str(aPerson.user.email)
        admininstitutedata['username'] = str(aPerson.user.username)
        admininstitutedata['fullname'] = str(aProfile.name)
        admininstitutedata['education'] = str([codes for codes in UserProfile.LEVEL_OF_EDUCATION_CHOICES if aProfile.level_of_education in codes][0][1]) or ''
        admininstitutedata['gender'] = str([codes for codes in UserProfile.GENDER_CHOICES if aProfile.gender in codes][0][1]) or ''
        admininstitutedata['mailing_address'] = str(aProfile.mailing_address)
        admininstitutedata['birth_date'] = str(aPerson.birth_date)
        admininstitutedata['contactnumber'] = ((aPerson.country_code) or '') + str(aPerson.mobile)

    except:
        raise
    
    js ={ 'success': True,
          'institutename': admininstitutedata.get('institutename', ''), 
          'email': admininstitutedata.get('email', ''),
          'username': admininstitutedata.get('username', ''),
          'fullname': admininstitutedata.get('fullname', ''),
          'education_level': admininstitutedata.get('education',''),
          'gender': admininstitutedata.get('gender',''),
          'mailing_address': admininstitutedata.get('mailing_address', '') ,
          'birth_date': admininstitutedata.get('birth_date', '') ,
          'contactnumber': admininstitutedata.get('contactnumber', ''),
          
        }

    return HttpResponse(json.dumps(js))


def admininstitute_approve(request,post_override=None):
    """
    This view will allow the Central coordinater to approve registered admininstitute
    """
    if not request.user.is_authenticated:
        raise Http404

    post_vars = post_override if post_override else request.POST
    admininstitute_id = post_vars['admininstitute_id']
    status = post_vars['admininstitute_status']

    try:
        Admininstitute_object = Admin_Institute.objects.filter(id=admininstitute_id)[0]
        status_id = int ( Institute_Status.objects.filter(name=status)[0].id )
        Admininstitute_object.status_id = status_id
        Admininstitute_object.save() 
         
    except:
        raise

    context= { }
    """
    if status=="Approved" :
        # composes Approve Admininstitute email
        subject = render_to_string('emails/approve_student_email_subject.txt',context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        message = render_to_string('emails/approve_student_email_body.txt',context)
    else :
        #composes Reject Admininstitute email
        subject = render_to_string('emails/reject_student_email_subject.txt',context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        message = render_to_string('emails/reject_student_email_body.txt',context)


    # don't send email if we are doing load testing or random user generation for some reason
    if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
        from_address = MicrositeConfiguration.get_microsite_configuration_value(
            'email_from_address',
            settings.DEFAULT_FROM_EMAIL
        )
        try:
            _res = Admininstitute_object.user.email_user(subject, message, from_address)

        except:
            log.warning('Unable to send Approve/Reject email to user', exc_info=True)
            js['value'] = _('Could not send Approve/Reject e-mail.')
            # What is the correct status code to use here? I think it's 500, because
            # the problem is on the server's end -- but also, the account was created.
            # Seems like the core part of the request was successful.
            return JsonResponse(js, status=500)     
    """
     
    return HttpResponse(json.dumps({'success': True}))


@login_required
@ensure_csrf_cookie
def institute_performance(request,post_override=None):
    '''
    This view will allow the central coordinater to view institute performance list.  
    '''
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
       
    except:
        if len(user.groups.all()) > 0:
	    user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0: 
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)	
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
                log.info("*************")
                log.info(rolename)
            except:
                rolename="Student"

    # for microsites, we want to filter and only show enrollments for courses within
    # the microsites 'ORG'
    course_org_filter = MicrositeConfiguration.get_microsite_configuration_value('course_org_filter')

    # Let's filter out any courses in an "org" that has been declared to be
    # in a Microsite
    org_filter_out_set = MicrositeConfiguration.get_all_microsite_orgs()  

    Institute_Objects=Institute_Registration.objects.all()
    Institute_Name_Approved = Institute_Registration.objects.filter(status=1) #status=1 indicates Status="Approved"    
    Student_Institute_Objects = Student_Institute.objects.filter(user=user)
    Faculty_Institute_Objects = Faculty_Institute.objects.filter(user=user)
    #user_course = Student_Institute_Objects.user;

    # remove our current Microsite from the "filter out" list, if applicable
    if course_org_filter:
        org_filter_out_set.remove(course_org_filter)

    # Build our (course, enrollment) list for the user, but ignore any courses that no
    # longer exist (because the course IDs have changed). Still, we don't delete those
    # enrollments, because it could have been a data push snafu.
    course_enrollment_pairs = list(get_course_enrollment_pairs(user, course_org_filter, org_filter_out_set))

    course_optouts = Optout.objects.filter(user=user).values_list('course_id', flat=True)

    message = ""
    if not user.is_active:
        #message = render_to_string('registration/activate_account_notice.html', {'email': user.email})
        return render_to_response("register_redirect.html",{'email': user.email})

    # Global staff can see what courses errored on their dashboard
    staff_access = False
    errored_courses = {}
    if has_access(user, 'global', 'staff'):
        # Show any courses that errored on load
        staff_access = True
        errored_courses = modulestore().get_errored_courses()

    show_courseware_links_for = frozenset(course.id for course, _enrollment in course_enrollment_pairs
                                          if has_access(request.user, course, 'load'))

    course_modes = {course.id: complete_course_mode_info(course.id, enrollment) for course, enrollment in course_enrollment_pairs}
    cert_statuses = {course.id: cert_info(request.user, course) for course, _enrollment in course_enrollment_pairs}

    # only show email settings for Mongo course and when bulk email is turned on
    show_email_settings_for = frozenset(
        course.id for course, _enrollment in course_enrollment_pairs if (
            settings.FEATURES['ENABLE_INSTRUCTOR_EMAIL'] and
            modulestore().get_modulestore_type(course.id) == MONGO_MODULESTORE_TYPE and
            CourseAuthorization.instructor_email_enabled(course.id)
        )
    )


    # Verification Attempts
    # Used to generate the "you must reverify for course x" banner
    verification_status, verification_msg = SoftwareSecurePhotoVerification.user_status(user)

    # Gets data for midcourse reverifications, if any are necessary or have failed
    statuses = ["approved", "denied", "pending", "must_reverify"]
    reverifications = reverification_info(course_enrollment_pairs, user, statuses)

    show_refund_option_for = frozenset(course.id for course, _enrollment in course_enrollment_pairs
                                       if _enrollment.refundable())

    # get info w.r.t ExternalAuthMap
    external_auth_map = None
    try:
        external_auth_map = ExternalAuthMap.objects.get(user=user)
    except ExternalAuthMap.DoesNotExist:
        pass

    # If there are *any* denied reverifications that have not been toggled off,
    # we'll display the banner
    denied_banner = any(item.display for item in reverifications["denied"])

   
    identity_name = "name" 
    context = {'course_enrollment_pairs': course_enrollment_pairs,
               'course_optouts': course_optouts,
               'message': message,
               'external_auth_map': external_auth_map,
               'staff_access': staff_access,
               'errored_courses': errored_courses,
               'show_courseware_links_for': show_courseware_links_for,
               'all_course_modes': course_modes,
               'cert_statuses': cert_statuses,
               'show_email_settings_for': show_email_settings_for,
               'reverifications': reverifications,
               'verification_status': verification_status,
               'verification_msg': verification_msg,
               'show_refund_option_for': show_refund_option_for,
               'denied_banner': denied_banner,
               'billing_email': settings.PAYMENT_SUPPORT_EMAIL,
               'rolename': rolename,
	       'Institute_Objects': Institute_Objects,
               'Student_Institute_Objects': Student_Institute_Objects,
               'Faculty_Institute_Objects': Faculty_Institute_Objects,
	       'identity_name' : identity_name,
               'Institute_Name_Approved' : Institute_Name_Approved
               }

    return render_to_response('institute_performance.html', context )


@ensure_csrf_cookie
@login_required
def local_faculty(request):
    """
    To become a local faculty, Person is redirected to this page for selecting his institute
    """
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"

    Institute_Objects=Institute_Registration.objects.all()
    Student_Institute_Objects = Student_Institute.objects.filter(user=user)
    Faculty_Institute_Objects = Faculty_Institute.objects.filter(user=user)
    context = {'rolename': rolename,
	       'Institute_Objects': Institute_Objects,
               'Student_Institute_Objects': Student_Institute_Objects,
               'Faculty_Institute_Objects': Faculty_Institute_Objects,
                }  
    return render_to_response("nav_local_faculty.html",context)

@login_required
@ensure_csrf_cookie
def faculty_ins_selection(request, post_override=None):
    """ 
    This view allows local faculty to select Institute in Person Dashboard 
    """
    if not request.user.is_authenticated:
        raise Http404

    user = request.user
    post_vars = post_override if post_override else request.POST
    fac_institute_id = post_vars['fac_ins_id']
    fac_course_id = post_vars['fac_course_id']
    log.info("*****")
    log.info(fac_institute_id)
    log.info("*****")
    log.info(fac_course_id)
    status=Institute_Status.objects.filter(name="Pending")[0].id

    try:    
        
	Faculty_Institute(user=user, institute_id=fac_institute_id,course_id=fac_course_id,status_id=status).save()    
	course_name = Course.objects.filter(id=fac_course_id)[0].name
        log.info(course_name)
        CourseEnrollment_object = CourseEnrollment.enroll(user,course_name,'honor')

        faculty_role_id = Role.objects.filter(name="Local Faculty")[0].id
        log.info(faculty_role_id)

        designation = Institute_Designation(user=user, institute_id=fac_institute_id, role_id=faculty_role_id, is_approved=False)
        designation.save()
    except IntegrityError:
        log.info("Integrity error")



    return HttpResponse(json.dumps({'success': True}))

@ensure_csrf_cookie
@login_required
def approve_faculty(request):
    """
    IC approve the person request to become local faculty.
    """
    user = request.user
    try:
        if user.is_superuser :
           rolename="Admin"
        else: 
           Central_Coordinator.objects.filter(user=user,is_approved=True)[0]    	            
           rolename="Central Coordinator"
    except:
        if len(user.groups.all()) > 0:
            user_creator = CourseCreator.objects.filter(user=user, state = CourseCreator.GRANTED)
            if user_creator.count() > 0:
                rolename = "Central Course Creator"
            elif len(Faculty_Institute.objects.filter(user=user,status_id=1)) > 0:
                rolename = "Local Faculty"
            else:
                rolename = "Central Course Assistant"
        else:
            try:
                role=Institute_Designation.objects.filter(user=user)
                role_names=Role.objects.filter(name=role[0].role)
                rolename=str(role_names[0])
            except:
                rolename="Student"

    Institute_Objects=Institute_Registration.objects.all()
    #Faculty_Institute_Objects = Faculty_Institute.objects.filter(user=user)
    Faculty_Institute_Objects = Faculty_Institute.objects.all()
    log.info("%%%%%%%%%%%%%%%%%%%%%")
    log.info(user)
    log.info(Faculty_Institute_Objects)
    context = {'rolename': rolename,
	           'Institute_Objects': Institute_Objects,
               'Faculty_Institute_Objects': Faculty_Institute_Objects,
                }  
    return render_to_response("ic_approve_faculty.html",context)

def faculty_approve(request,post_override=None):
    """
    This view will allow the Institute coordinater to approve registered faculty
    """
    if not request.user.is_authenticated:
        raise Http404


    user = request.user
    post_vars = post_override if post_override else request.POST
    faculty_id = post_vars['faculty_id']
    user_id = post_vars['faculty_user_id']
    faculty_course_id = post_vars['faculty_course_id']
    status = post_vars['faculty_status']
    log.info("$$$$$$$$$$$$$")
    log.info(user_id)
    log.info(status)
    log.info(faculty_course_id)

    try:
        Faculty_object = Faculty_Institute.objects.filter(id=faculty_id)[0]
        status_id = int ( Institute_Status.objects.filter(name=status)[0].id )
        log.info(status_id)
        log.info(faculty_id)
        Faculty_object.status_id = status_id
        Faculty_object.save() 
        
        # Adding selected course to course enrollment table after aprpoving as faculty.
        course_name = Course.objects.filter(id=faculty_course_id)[0].name
        log.info(course_name)
        log.info(user)
	user_faculty = User.objects.filter(id=user_id)[0]
        CourseEnrollment_object = CourseEnrollment.enroll(user_faculty,course_name,'honor')
        
        # Adding faculty to course staff and instructor access
	
        course_name_staff_group =  ("staff_"+course_name.replace("/",".")).lower()
	course_name_instructor_group =  ("instructor_"+course_name.replace("/",".")).lower()
        log.info("^^^^^^^^^^^^^^^^^^")
        log.info(course_name_staff_group)
        log.info(course_name_instructor_group)
 
        auth_staff_group_id = Group.objects.filter(name=course_name_staff_group)[0].id
        auth_instructor_group_id = Group.objects.filter(name=course_name_instructor_group)[0].id
	log.info(auth_staff_group_id)
	log.info(auth_instructor_group_id)
     
        user_faculty.groups.add(auth_staff_group_id)
        user_faculty.groups.add(auth_instructor_group_id)
       
	faculty_role_id = Role.objects.filter(name="Local Faculty")[0].id
        log.info(faculty_role_id)
 
        #designation = Institute_Designation(user=user, institute_id=insti_id, role_id=faculty_role_id, is_approved=False)
        #designation.save()

         
    except IntegrityError:
        log.info("Integrity error")
    return HttpResponse(json.dumps({'success': True}))


@ensure_csrf_cookie
@login_required
def fetch_course_details(request):
    """
    This view will allow the central coordinater to view performance and course details of a particular institute.
    """

    js ={ 
        }

    return HttpResponse(json.dumps(js))

@login_required
@ensure_csrf_cookie
def send_cc_mail(request, post_override=None):
    """ 
    This view allows Central Coordinator to send E-MAil
    """
    if not request.user.is_authenticated:
        raise Http404

    user = request.user
    post_vars = post_override if post_override else request.POST

    to = post_vars['to']
    subject_mail = post_vars['subject']
    body = post_vars['body']  

    # composes E-Mail
    subject = subject_mail
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    message = body
    to_object = User.objects.filter(email=to)[0]
    
    
    # don't send email if we are doing load testing or random user generation for some reason
    if not (settings.FEATURES.get('AUTOMATIC_AUTH_FOR_TESTING')):
        from_address = 'iitbombayxcc@cse.iitb.ac.in'
        
        try:
             
             _res = to_object.email_user(subject, message, from_address)

        except:
            log.warning('Unable to send', exc_info=True)
            js['value'] = _('Could not send e-mail.')
            # What is the correct status code to use here? I think it's 500, because
            # the problem is on the server's end -- but also, the account was created.
            # Seems like the core part of the request was successful.
            return JsonResponse(js, status=500)     

    return HttpResponse(json.dumps({'success': True}))


#Synchronization Code

'''
Enumerations:
'central_query_User_ins_list' as 1
'central_query_User_del_list' as 2 
'central_query_UserProfile_ins_list' as 3
'central_query_UserProfile_del_list' as 4
'central_query_Person_ins_list' as 5
'central_query_Person_del_list' as 6
'central_query_Institute_Designation_ins_list' as 7
'central_query_Institute_Designation_del_list' as 8
''' 

dict_hash = {}


#start_hash = {'Person': 'bcd8b0c2eb1fce714eab6cef0d771acc', 'Institute_Designation': 'bcd8b0c2eb1fce714eab6cef0d771acc', 'User': 'bcd8b0c2eb1fce714eab6cef0d771acc', 'UserProfile': 'bcd8b0c2eb1fce714eab6cef0d771acc'}


#Method which writes the selective data that belongs to a institute to that particular table file.
#If file doesn't exists it creates and if it exists it overwrites
#File Name are as table name + previous and are stored in respective institute directory
'''def write_to_file(central_prev_query_table_tuple,table_name,institute_id):
    path = str(settings.REPO_ROOT) + "/syncing/institute" + str(institute_id)
    filename = path + "/" + table_name.__name__ + "_prev"
    f = open(filename , 'w')
    f.write(str(central_prev_query_table_tuple))
    f.close()


#Method to generate the selective data that belongs to an institute.
#Input is institute_id, table_name and the values(coloumns which are required to synchronized)
def get_filtered_data(table_name,institute_id,values):
    temp = User.objects.filter(Q(student_institute__institute_id = institute_id) | Q( institute_designation__institute_id = institute_id) )
    if table_name == User:
        return temp.values_list(*values)
    else:
        temporary = table_name.objects.filter(user__id__in = temp ).values_list(*values)
        temp_str = str(temporary)
        if "datetime.date" in temp_str:
            temp_str = re.sub(r'(datetime.date\()(\d+)(, )(\d+)(, )(\d+)(\))',r"',temp_str)
            temporary = ast.literal_eval(temp_str)
        return temporary



#This method generates the tuples and hashes of the filtered data and stores hash into a global variable
def generate(table_name,institute_id,values):
    global dict_hash
    central_prev_query_table = get_filtered_data(table_name,institute_id,values)
    central_prev_query_table_tuple = tuple(central_prev_query_table)
    central_prev_query_table_tuple_hash = hashlib.md5(str(central_prev_query_table_tuple)).hexdigest()
    dict_hash[ table_name.__name__ ] = central_prev_query_table_tuple_hash
    print dict_hash
    return central_prev_query_table_tuple


#This method takes the institute_id and here we have mentioned the tables which we need to synchronize with the columns required and the foreign key requirements.
#Calls the generate function to generate the selective data, writes that data to file and in the end the hash of the tables are stored into a file named Hash into respective institute directory.
def initialize_part(institute_id):
    central_prev_query_table_tuple = generate(User,institute_id,['id','username','email',  'password' , 'is_active'])
    write_to_file(central_prev_query_table_tuple,User,institute_id)


    central_prev_query_table_tuple = generate(UserProfile,institute_id,['id','user_id','name','year_of_birth','gender','level_of_education','mailing_address','allow_certificate'])
    write_to_file(central_prev_query_table_tuple,UserProfile,institute_id)

    central_prev_query_table_tuple = generate(Person,institute_id,['id','user_id','birth_date','mobile'])
    write_to_file(central_prev_query_table_tuple,Person,institute_id)


    central_prev_query_table_tuple = generate(Institute_Designation,institute_id,['id','user_id','institute_id','role_id','is_approved'])
    write_to_file(central_prev_query_table_tuple,Institute_Designation,institute_id)

    hash_file_path = "syncing/institute" + str(institute_id) + "/Hash"
    f = open(hash_file_path, 'w')
    f.write(str(dict_hash))
    f.close()



#This method is to generate the directory and files required for synchronization when an institute registers
#command initialize_institute generates the directory
#initialize_part generates the files with selective data based on institute id
def initialize(institute_id):
    call_command('initialize_institute',institute_id)
    initialize_part(institute_id)



#This methods reads the data from the files related to that institute and table
def read_prev_data_from_file(table_name,institute_id):
        path = "syncing/institute" + str(institute_id)
        filename = path + "/" + table_name.__name__ + "_prev"
        f = open(filename, 'r')
        temp = f.read()
        if "datetime.date" in temp:
            temp = re.sub(r'(datetime.date\()(\d+)(, )(\d+)(, )(\d+)(\))',r'"\2-\4-\6"',temp)
        temp = ast.literal_eval(temp)
        f.close()
        return temp

#This generates the insert/update and delete list to be send to institute
def get_ins_del_list(prev_data,new_data):

    central_prev_query_list  = list( list(i) for i in prev_data )
    central_new_query_list  = list( list(i) for i in new_data )

    central_query_ins_list = [ x for x in central_new_query_list if x not in central_prev_query_list ]
    central_query_del_list = [ x for x in central_prev_query_list if x not in central_new_query_list ]

    for x in central_query_del_list:
        for y in central_query_ins_list:
            if x[0] == y[0]:
                 central_query_del_list.remove(x)

    central_query_del_list = [ [x[0]] for x in central_query_del_list ]


    return central_query_ins_list , central_query_del_list

'''
'''Institute sends its id ans data hashes, So we regenerate the data that we have belonging to the institute and generates their hash
Then hashes are matched , if yes data is already up to date and we update our hash file , else we filter out what data needs to be sent.
In else we matche the hashes with the hashes stored in the file, if they are matched we have current data set and the data set which institute have, in this case read data from files and find the differences.
Else we believe institute has lost the synchornization and we don't know at what data is currently present over there, so we send all the institute specific data to the institute server.

'''
'''def institute_request(institute_id,institute_hash_dict):
    global dict_hash
    central_new_query_User_tuple = generate(User,institute_id,['id','username','email',  'password' , 'is_active'])
    central_new_query_UserProfile_tuple = generate(UserProfile,institute_id,['id','user_id','name','year_of_birth','gender','level_of_education','mailing_address','allow_certificate'])
    central_new_query_Person_tuple = generate(Person,institute_id,['id','user_id','birth_date','mobile'])
    central_new_query_Institute_Designation_tuple = generate(Institute_Designation,institute_id,['id','user_id','institute_id','role_id','is_approved'])


    #Match hash with current data
    if dict_hash == institute_hash_dict:
        initialize_part(institute_id)
        log.info("Up-to-Date")
        return {"data":"Already Up-to-date"}

    else:

        hash_file_path = "syncing/institute" + str(institute_id) + "/Hash"
        f = open(hash_file_path, 'r')
        temp = f.read()
        dict_hash = ast.literal_eval(temp)
        f.close()

        if dict_hash == institute_hash_dict:
            log.info("Matched From File")
            central_prev_query_User_tuple = read_prev_data_from_file(User,institute_id)
            central_prev_query_UserProfile_tuple = read_prev_data_from_file(UserProfile,institute_id)
            central_prev_query_Person_tuple = read_prev_data_from_file(Person,institute_id)
            central_prev_query_Institute_Designation_tuple = read_prev_data_from_file(Institute_Designation,institute_id)

        else:

            central_prev_query_User_tuple = ()
            central_prev_query_UserProfile_tuple = ()
            central_prev_query_Person_tuple = ()
            central_prev_query_Institute_Designation_tuple = ()

        central_query_User_ins_list, central_query_User_del_list = get_ins_del_list(central_prev_query_User_tuple,central_new_query_User_tuple)
        central_query_UserProfile_ins_list, central_query_UserProfile_del_list = get_ins_del_list(central_prev_query_UserProfile_tuple,central_new_query_UserProfile_tuple)
        central_query_Person_ins_list, central_query_Person_del_list = get_ins_del_list(central_prev_query_Person_tuple,central_new_query_Person_tuple)
        central_query_Institute_Designation_ins_list, central_query_Institute_Designation_del_list = get_ins_del_list(central_prev_query_Institute_Designation_tuple,central_new_query_Institute_Designation_tuple)
           
        temp = {}
        temp['1'] = central_query_User_ins_list
        temp['2'] = central_query_User_del_list
        temp['3'] = central_query_UserProfile_ins_list
        temp['4'] = central_query_UserProfile_del_list
        temp['5'] = central_query_Person_ins_list
        temp['6'] = central_query_Person_del_list
        temp['7'] = central_query_Institute_Designation_ins_list
        temp['8'] = central_query_Institute_Designation_del_list
        log.info("Institute {}".format(temp))
        return temp

#This method is called when an institute requests for the synchronization, we reads all the hashes sent by institute
def sync_institute(request):
    institute_hash_dict = {}
    institute_id = request.GET.get("institute_id")
    log.info("Synchronizing Institute {}".format(institute_id))
    institute_hash_dict['User'] = request.GET.get("User")
    institute_hash_dict['UserProfile'] = request.GET.get("UserProfile")
    institute_hash_dict['Person'] = request.GET.get("Person")
    institute_hash_dict['Institute_Designation'] = request.GET.get("Institute_Designation")
    temp = institute_request(institute_id,institute_hash_dict)
    return JsonResponse(temp)


'''









#New Synchronization Code

'''def institute_request_new(institute_id,institute_hash_dict):
    global dict_hash
    central_new_query_User_tuple = generate(User,institute_id,['id','username','email',  'password' , 'is_active'])
    central_new_query_UserProfile_tuple = generate(UserProfile,institute_id,['id','user_id','name','year_of_birth','gender','level_of_education','mailing_address','allow_certificate'])
    central_new_query_Person_tuple = generate(Person,institute_id,['id','user_id','birth_date','mobile'])
    central_new_query_Institute_Designation_tuple = generate(Institute_Designation,institute_id,['id','user_id','institute_id','role_id','is_approved'])


    #Match hash with current data
    if dict_hash == institute_hash_dict:
        log.info("Up-to-Date")
        return {"data":"Already Up-to-date"}
    else:
	temp = {}

	data = []
        find_id = Institute_Sync.objects.order_by('id').filter(institute_id=institute_id, model_name="User",change_type=False ).values_list('record')
	for item in find_id:
		data.append(list(User.objects.filter(id=item[0]).values_list('id','username','email',  'password' , 'is_active')[0]))
	temp['1']  = data


	data = []
        find_id = Institute_Sync.objects.order_by('id').filter(institute_id=institute_id, model_name="UserProfile",change_type=False ).values_list('record')
	for item in find_id:
		data.append(list(UserProfile.objects.filter(id=item[0]).values_list('id','user_id','name','year_of_birth','gender','level_of_education','mailing_address','allow_certificate')[0]))
	temp['3']  = data


	data = []
        find_id = Institute_Sync.objects.order_by('id').filter(institute_id=institute_id, model_name="Person",change_type=False ).values_list('record')
	for item in find_id:
		data.append(list(Person.objects.filter(id=item[0]).values_list('id','user_id','birth_date','mobile')[0]))
	temp['5']  = data

	data = []
        find_id = Institute_Sync.objects.order_by('id').filter(institute_id=institute_id, model_name="Institute_Designation",change_type=False ).values_list('record')
	for item in find_id:
		data.append(list(Institute_Designation.objects.filter(id=item[0]).values_list('id','user_id','institute_id','role_id','is_approved')[0]))
        temp['7']=data

	temp['2'] = []
	temp['4'] = []
	temp['6'] = []
	temp['8'] = []
        log.info("Institute {}".format(temp))
        return temp

#This method is called when an institute requests for the synchronization, we reads all the hashes sent by institute
def sync_institute_new(request):
    institute_hash_dict = {}
    institute_id = request.GET.get("institute_id")
    log.info("Synchronizing Institute {}".format(institute_id))
    institute_hash_dict['User'] = request.GET.get("User")
    institute_hash_dict['UserProfile'] = request.GET.get("UserProfile")
    institute_hash_dict['Person'] = request.GET.get("Person")
    institute_hash_dict['Institute_Designation'] = request.GET.get("Institute_Designation")
    temp = institute_request_new(institute_id,institute_hash_dict)
    return JsonResponse(temp)'''
