"""
Utility functions for validating forms
"""
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.hashers import UNUSABLE_PASSWORD

"""
import re
from student.models import Document
from mooc.models import Course, Institute_Course, Institute_Identity
from smart_selects.form_fields import ChainedModelChoiceField
"""

class PasswordResetFormNoActive(PasswordResetForm):
    def clean_email(self):
        """
        This is a literal copy from Django 1.4.5's django.contrib.auth.forms.PasswordResetForm
        Except removing the requirement of active users
        Validates that a user exists with the given email address.
        """
        email = self.cleaned_data["email"]
        #The line below contains the only change, removing is_active=True
        self.users_cache = User.objects.filter(email__iexact=email)
        if not len(self.users_cache):
            raise forms.ValidationError(self.error_messages['unknown'])
        if any((user.password == UNUSABLE_PASSWORD)
               for user in self.users_cache):
            raise forms.ValidationError(self.error_messages['unusable'])
        return email

"""
class DocumentForm(forms.Form):
    ''' 
       This form view details which institute is accepting which identities. 
       with identitiy proof - Document File upload  
    '''

    error_messages_doc = {
        'acourse': ("Select a Course"),
        'ainstitute_course': ("Select a Institute"),
        'ainstitute_identity': ("Select an Identity Proof"),
        'aproofnumber': ("Enter a valid ID Proof number"), 
        'adocfile': ("Upload a file in .jpg, .jpeg, .png, .gif format"),
        'adocfilesize': ("Please keep filesize under 2.5MB"),
    }

    course = forms.ModelChoiceField(queryset=Course.objects.all(), label='Course', empty_label="Select Course")
    institute = forms.ChoiceField(choices=(), label='Institute Name', widget=forms.Select(attrs={'class':'selector'}) )
    identity = forms.ModelChoiceField(queryset=Institute_Identity.objects.all(),label='Identity Proof',empty_label="Select Identity Proof")
    proofnumber = forms.CharField(max_length=100, label='Enter Proof ID')
    docfile = forms.FileField(label='Select a file')
    filename = forms.CharField(widget=forms.HiddenInput())
    
    class Meta:
        model = Document
        fields = ['course', 'institute', 'identity', 'proofnumber', 'docfile', 'filename']
 
    def __init__(self, *args, **kwargs):
        super(DocumentForm, self).__init__(*args, **kwargs)
        self.content_types = ['image/jpg', 'image/jpeg' , 'image/png' , 'image/gif' ]
        self.max_upload_size = 2621440 # 1024 # 2.5MB 
        self.fields['filename'].initial = 'None.png'
        self.fields['course'].error_messages = {'required': 'Select a Course'} 
        self.fields['institute'].error_messages = {'required': 'Select a Institute'}
        self.fields['identity'].error_messages = {'required': 'Select an Identity Proof'}
        self.fields['proofnumber'].error_messages = {'required': 'Enter a valid ID Proof number'}
        self.fields['docfile'].error_messages = {'required': 'Choose a file to upload'}
        choices = [(int(ic.institute_id), unicode(ic.institute_name)) for ic in Institute_Course.objects.all()]
        self.fields['institute'].choices = choices
 
    def clean_docfile(self):
        docfile = self.cleaned_data['docfile']   

        try:
            content_type = docfile.content_type
            if (content_type in self.content_types): 
                if docfile.size > self.max_upload_size: # 
                    raise forms.ValidationError(self.error_messages_doc['adocfilesize'])
            else:
                raise forms.ValidationError(self.error_messages_doc['adocfile'])
             
        except Exception as e:
            raise forms.ValidationError(self.error_messages_doc['adocfile']) 

        
        # your other validations go here.
        return docfile


    def clean_course(self):
        course = None
        try:
            course = self.cleaned_data['course']
            if course and course in [None, '', 'Select Course',]:
                raise forms.ValidationError("Select a Course")
        except Exception as e:
            raise forms.ValidationError("Select a Course") 

         
        # your other validations go here.
        return course

    
    def clean_institute(self):
        institute = None
        try:
            institute = self.cleaned_data['institute']
            if institute and institute in [None, '', 'Select Institute',]:
                raise forms.ValidationError("Select a Institute")
        except Exception as e:
            raise forms.ValidationError("Select a Institute") 

         
        # your other validations go here.
        return institute

    
    def clean_identity(self):
        identity = None
        try:
            identity = self.cleaned_data['identity']
            if identity and identity in [None, '', 'Select Identity Proof',]:
                raise forms.ValidationError("Select an Identity Proof")
        except Exception as e:
            raise forms.ValidationError("Select an Identity Proof") 

         
        # your other validations go here.
        return identity
     

    def clean_proofnumber(self):
        proofnumber = None
        try:
            proofnumber = self.cleaned_data['proofnumber']
            if proofnumber:
                if len(proofnumber) == 0 :
                   raise forms.ValidationError("Enter a valid ID Proof number")
                valid = re.match('^[\w-]+$', proofnumber) is not None
                if valid is False:
                   raise forms.ValidationError("Enter a valid ID Proof number")
                 
        except Exception as e:
            raise forms.ValidationError("Enter a valid ID Proof number") 

         
        # your other validations go here.
        return proofnumber
"""

    
    
