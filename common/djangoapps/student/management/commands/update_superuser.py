from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from student.models import UserProfile

from track.management.tracked_command import TrackedCommand


class Command(TrackedCommand):
    help = """
    This command updates the profile of superuser.
    """

    option_list = BaseCommand.option_list + (
        make_option('-n', '--name',
                    metavar='NAME',
                    dest='name',
                    default=None,
                    help='Name of SuperUser'),
        make_option('-e', '--email',
                    metavar='EMAIL',
                    dest='email',
                    default=None,
                    help='Email of SuperUser'),
    )

    def handle(self, *args, **options):
        name = options['name']
        email = options['email']
        if not name:
            raise CommandError("Plese provide Full Name with -n 'name' ")
        if not email:
            raise CommandError("Plese provide Email with -e 'email' ")

        try:
            superuser_object = User.objects.filter(email=email)[0]
            if superuser_object.is_superuser == 1:
                if UserProfile.objects.filter(user_id = superuser_object.id):
                    userprofile_object = UserProfile.objects.filter(user_id = superuser_object.id)[0]
                    userprofile_object.name = name
                    userprofile_object.save()
                else:
                    UserProfile(user_id = superuser_object.id , name=name).save()
            else:
                raise CommandError("You are not Superuser, Don't try to be Oversmart")
        except:
            raise
